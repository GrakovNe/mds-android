package org.grakovne.mds.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.utils.CommonUtils.Action;

public class ConfirmDialog {
    private final Context context;
    private final String title;
    private final String message;
    private final Action action;

    public ConfirmDialog(Context context, String title, String message, Action action) {
        this.context = context;
        this.title = title;
        this.message = message;
        this.action = action;

        show();
    }

    private void show() {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.confirm_dialog_yes, this::whenConfirm)
                .setNegativeButton(R.string.confirm_dialog_no, this::whenCancel)
                .setCancelable(false)
                .create()
                .show();
    }


    private void whenConfirm(DialogInterface dialog, int which) {
        action.apply();
    }

    private void whenCancel(DialogInterface dialog, int which) {
        dialog.cancel();
    }


}
