package org.grakovne.mds.android.network.domain.forms;

import java.io.Serializable;

public class BookmarkForm implements Serializable {
    private Integer timestamp;

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
}
