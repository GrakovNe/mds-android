package org.grakovne.mds.android.service;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ExternalReceiver;
import org.grakovne.mds.android.common.PayloadNames;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.network.domain.responses.Story;

import static org.grakovne.mds.android.common.ActionNames.Cache;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CacheUtils.buildStreamFile;
import static org.grakovne.mds.android.utils.CacheUtils.isStreamCached;
import static org.grakovne.mds.android.utils.NetworkUtils.findStreamUrl;

public class CacheService extends MdsService {

    private Receiver receiver;
    private DownloadManager manager;

    @Override
    protected void init() {
        super.init();
        manager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
    }

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                Cache.CACHE_REMOVED_STREAM_INTENT,
                Cache.STREAM_IS_CACHED_INTENT,
                Cache.CACHED_STREAM_INTENT,
                Cache.FOUND_STREAM_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Cache.CACHED_STREAM_INTENT:
                        saveStream(getPayloadObject(intent, PayloadNames.STORY, Story.class));
                        break;
                    case Cache.CACHE_REMOVED_STREAM_INTENT:
                        removeStream(getPayloadObject(intent, PayloadNames.STORY, Story.class));
                        break;
                    case Cache.FOUND_STREAM_INTENT:
                        findStream(getPayloadObject(intent, PayloadNames.STORY, Story.class));
                        break;
                    case Cache.STREAM_IS_CACHED_INTENT:
                        isStreamCached(CacheService.this, getPayloadObject(intent, PayloadNames.STORY, Story.class));
                }
            }
        };
    }

    private void findStream(Story story) {
        if (isStreamCached(this, story)) {
            send(this, Cache.FOUND_STREAM_EVENT, buildStreamFile(this, story));
        }
    }

    private void saveStream(Story story) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(findStreamUrl(story)));
        request.setTitle(story.getTitle());
        request.setDescription(getString(R.string.cache_story_downloading_title));
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(Uri.fromFile(buildStreamFile(this, story)));

        manager.enqueue(request);
        new ExternalReceiver(this, DownloadManager.ACTION_DOWNLOAD_COMPLETE) {
            @Override
            public void onAction(Intent intent, String action) {
                this.unsubscribe();
                send(CacheService.this, Cache.CACHED_STREAM_EVENT, story);
            }
        };
    }

    private void removeStream(Story story) {
        if (buildStreamFile(this, story).delete()) {
            send(this, Cache.CACHE_REMOVED_STREAM_EVENT, story);
        }
    }

    @Override
    protected void unsubscribe() {
        receiver.unsubscribe();
        super.unsubscribe();
    }
}
