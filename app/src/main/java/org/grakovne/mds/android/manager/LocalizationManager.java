package org.grakovne.mds.android.manager;

import android.content.Context;
import android.text.TextUtils;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.StoryTab;

import java.util.HashMap;
import java.util.Map;

public class LocalizationManager {

    private static LocalizationManager instance;

    private final Map<String, String> tabType;
    private final String appName;

    private LocalizationManager(Context context) {
        tabType = new HashMap<>();
        tabType.put(StoryTab.BEST_STORIES, context.getString(R.string.drawer_item_best_stories));
        tabType.put(StoryTab.RECENT_STORIES, context.getString(R.string.drawer_item_recent_stories));
        tabType.put(StoryTab.ALL_STORIES, context.getString(R.string.drawer_item_all_stories));
        appName = context.getString(R.string.full_application_name);
    }

    public static LocalizationManager getManager(Context context) {
        if (null == instance) {
            instance = new LocalizationManager(context);
        }

        return instance;
    }

    public String getStoryType(String storyType) {
        String result = tabType.get(storyType);
        return TextUtils.isEmpty(result) ? appName : result;
    }
}
