package org.grakovne.mds.android.manager;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

public abstract class DataManager {
    protected final Gson gson = new Gson();
    protected SharedPreferences sharedPreferences;

    protected void putString(SharedPreferences.Editor editor, String key, String value) {
        putString(editor, key, value, "");
    }

    protected void putString(SharedPreferences.Editor editor, String key, String value, String defaultValue) {
        editor.putString(key, TextUtils.isEmpty(value) ? defaultValue : value);
    }

    protected void putInteger(SharedPreferences.Editor editor, String key, Integer value, Integer defaultValue) {
        editor.putInt(key, null == value ? defaultValue : value);
    }

    protected void putInteger(SharedPreferences.Editor editor, String key, Integer value) {
        putInteger(editor, key, value, 0);
    }

    protected void putBoolean(SharedPreferences.Editor editor, String key, Boolean value, Boolean defaultValue) {
        editor.putBoolean(key, null == value ? defaultValue : value);
    }

    protected void putBoolean(SharedPreferences.Editor editor, String key, Boolean value) {
        putBoolean(editor, key, value, false);
    }
}
