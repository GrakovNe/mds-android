package org.grakovne.mds.android.utils;

import org.grakovne.mds.android.media.DataSource;
import org.grakovne.mds.android.network.common.ApiNames;
import org.grakovne.mds.android.network.domain.responses.Story;

public class NetworkUtils {

    public static String findStoryUrl(DataSource source) {
        return ApiNames.getApiUrl() + "story/" + source.getStory().getId() + "/stream?offset=" + source.getOffset();
    }

    public static String findStreamUrl(Story story) {
        return ApiNames.getApiUrl() + "story/" + story.getId() + "/stream";
    }

    public static String findImageUrl(Story story) {
        return ApiNames.getApiUrl() + "story/" + story.getId() + "/cover";
    }
}
