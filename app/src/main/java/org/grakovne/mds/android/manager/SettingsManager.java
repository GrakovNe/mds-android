package org.grakovne.mds.android.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.grakovne.mds.android.common.enums.BookmarksConsidering;

import static org.grakovne.mds.android.common.SearchParams.BOTH;
import static org.grakovne.mds.android.common.enums.BookmarksConsidering.DEFAULT;

public class SettingsManager extends DataManager {

    private static final String BASE_ACTION_PREFIX = "org.grakovne.mds.android.";
    private static final String APPLICATION_PREFERENCES = BASE_ACTION_PREFIX + "application_preferences";
    private static final String SETTINGS_PLAYING_IS_PLAYING = BASE_ACTION_PREFIX + "settings_playing_story_is_playing";
    private static final String SETTINGS_TIMER_IS_ENABLED = BASE_ACTION_PREFIX + "settings_timer_is_enabled";
    private static final String SETTINGS_BOOKMARKS_CONSIDERING_TYPE = BASE_ACTION_PREFIX + "settings_bookmarks_considering_type";
    private static final String SETTINGS_IS_AUTO_PLAY = BASE_ACTION_PREFIX + "settings_is_auto_play";
    private static final String SEARCH_STORY_LISTENED_TYPE = BASE_ACTION_PREFIX + "search_story_listened_type";
    private static SettingsManager instance;

    private SettingsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(APPLICATION_PREFERENCES, Context.MODE_PRIVATE);
        setTimerEnabled(null);
    }

    public static Boolean isDebugEnabled() {
        return false;
    }

    public static SettingsManager getManager(Context context) {

        if (null == instance) {
            instance = new SettingsManager(context);
        }

        return instance;
    }

    public Boolean isStoryPlaying() {
        return sharedPreferences.getBoolean(SETTINGS_PLAYING_IS_PLAYING, false);
    }

    public Boolean isAutoPlay() {
        return sharedPreferences.getBoolean(SETTINGS_IS_AUTO_PLAY, true);
    }

    public void setAutoPlay(Boolean isAutoPlay) {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(SETTINGS_IS_AUTO_PLAY, isAutoPlay == null ? true : isAutoPlay);
        editor.apply();
    }

    public void setStoryPlaying(Boolean isStoryPlaying) {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(SETTINGS_PLAYING_IS_PLAYING, isStoryPlaying == null ? false : isStoryPlaying);
        editor.apply();
    }

    public Boolean isTimerEnabled() {
        return sharedPreferences.getBoolean(SETTINGS_TIMER_IS_ENABLED, false);
    }

    public void setTimerEnabled(Boolean isTimerEnabled) {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(SETTINGS_TIMER_IS_ENABLED, isTimerEnabled == null ? false : isTimerEnabled);
        editor.apply();
    }

    public BookmarksConsidering getBookmarksConsideringType() {
        return BookmarksConsidering.valueOf(sharedPreferences.getString(SETTINGS_BOOKMARKS_CONSIDERING_TYPE, DEFAULT.name()));
    }

    public void setBookmarksConsideringType(BookmarksConsidering type) {
        Editor editor = sharedPreferences.edit();
        editor.putString(SETTINGS_BOOKMARKS_CONSIDERING_TYPE, type == null ? DEFAULT.name() : type.name());
        editor.apply();
    }

    public String getSearchStoryListenedType() {
        return sharedPreferences.getString(SEARCH_STORY_LISTENED_TYPE, BOTH);
    }

    public void setSearchStoryListenedType(String listenedType) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, SEARCH_STORY_LISTENED_TYPE, listenedType);
        editor.apply();
    }
}
