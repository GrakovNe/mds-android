package org.grakovne.mds.android.media;

import java.io.Serializable;

public class TimerSettings implements Serializable {
    private Integer hours;
    private Integer minutes;
    private Integer seconds;

    public TimerSettings(Integer hours, Integer minutes, Integer seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    private Integer getSeconds() {
        return hours * 3600 + minutes * 60 + seconds;
    }

    public Integer getMilliseconds() {
        return getSeconds() * 1000;
    }
}
