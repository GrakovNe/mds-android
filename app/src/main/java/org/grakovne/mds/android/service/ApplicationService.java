package org.grakovne.mds.android.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames;
import org.grakovne.mds.android.common.ActionNames.Service;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.network.service.StoryService;
import org.grakovne.mds.android.network.service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.grakovne.mds.android.manager.BroadcastManager.send;

public class ApplicationService extends MdsService {

    private final List<Class<? extends MdsService>> services = Arrays.asList(
            StoryService.class,
            UserService.class,
            NotificationService.class,
            RouterService.class,
            PlayerService.class,
            MediaButtonService.class,
            GateService.class,
            CacheService.class,
            TimerService.class
    );
    private AtomicInteger serviceCounter;
    private Receiver receiver;

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this, Service.STARTED_EVENT) {
            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Service.STARTED_EVENT:
                        serviceStartedAction();
                        break;
                }
            }
        };
    }

    private void serviceStartedAction() {
        if (services.size() == serviceCounter.incrementAndGet()) {
            send(this, ActionNames.Application.STARTED_EVENT);
        }
    }

    @Override
    public void init() {
        serviceCounter = new AtomicInteger(0);

        for (Class<? extends MdsService> service : services) {
            startService(new Intent(this, service));
        }
    }

    @Override
    public void unsubscribe() {
        for (Class<? extends MdsService> service : services) {
            stopService(new Intent(this, service));
        }

        receiver.unsubscribe();
        stopSelf();
    }
}
