package org.grakovne.mds.android.common;

public class ActionNames {
    private static final String BASE_ACTION_PREFIX = "org.grakovne.mds.android.";

    public static class Player {
        private static final String PLAYER = "PLAYER.";

        public static final String STARTED_INTENT = BASE_ACTION_PREFIX + PLAYER + "STARTED_INTENT";
        public static final String STARTED_EVENT = BASE_ACTION_PREFIX + PLAYER + "STARTED_EVENT";

        public static final String FINISHED_INTENT = BASE_ACTION_PREFIX + PLAYER + "FINISHED_INTENT";
        public static final String FINISHED_EVENT = BASE_ACTION_PREFIX + PLAYER + "FINISHED_EVENT";

        public static final String PLAYED_INTENT = BASE_ACTION_PREFIX + PLAYER + "PLAYED_INTENT";
        public static final String PLAYED_EVENT = BASE_ACTION_PREFIX + PLAYER + "PLAYED_EVENT";

        public static final String PAUSED_INTENT = BASE_ACTION_PREFIX + PLAYER + "PAUSED_INTENT";
        public static final String PAUSED_EVENT = BASE_ACTION_PREFIX + PLAYER + "PAUSED_EVENT";

        public static final String STOPPED_INTENT = BASE_ACTION_PREFIX + PLAYER + "STOPPED_INTENT";
        public static final String STOPPED_EVENT = BASE_ACTION_PREFIX + PLAYER + "STOPPED_EVENT";

        public static final String PREPARED_EVENT = BASE_ACTION_PREFIX + PLAYER + "PREPARED_EVENT";

        public static final String SOUGHT_INTENT = BASE_ACTION_PREFIX + PLAYER + "SOUGHT_INTENT";
        public static final String SOUGHT_EVENT = BASE_ACTION_PREFIX + PLAYER + "SOUGHT_EVENT";

        public static final String SENT_STATE_INFO_INTENT = BASE_ACTION_PREFIX + PLAYER + "SENT_STATE_INFO_INTENT";
        public static final String SENT_STATE_INFO_EVENT = BASE_ACTION_PREFIX + PLAYER + "SENT_STATE_INFO_EVENT";
    }

    public static class Notification {
        private static final String NOTIFICATION = "NOTIFICATION.";

        public static final String PRESSED_EVENT = BASE_ACTION_PREFIX + NOTIFICATION + "PRESSED_EVENT";

        public static final String GOT_INTENT = BASE_ACTION_PREFIX + NOTIFICATION + "GOT_INTENT";
        public static final String GOT_EVENT = BASE_ACTION_PREFIX + NOTIFICATION + "GOT_EVENT";
    }

    public static class Cache {
        private static final String CACHE = "CACHE.";

        public static final String CACHED_STREAM_INTENT = BASE_ACTION_PREFIX + CACHE + "CACHED_STREAM_INTENT";
        public static final String STREAM_IS_CACHED_INTENT = BASE_ACTION_PREFIX + CACHE + "STREAM_IS_CACHED_INTENT";
        public static final String FOUND_STREAM_INTENT = BASE_ACTION_PREFIX + CACHE + "FOUND_STREAM_INTENT";
        public static final String CACHE_REMOVED_STREAM_INTENT = BASE_ACTION_PREFIX + CACHE + "CACHE_REMOVED_STREAM_INTENT";

        public static final String CACHED_STREAM_EVENT = BASE_ACTION_PREFIX + CACHE + "CACHED_STREAM_EVENT";
        public static final String FOUND_STREAM_EVENT = BASE_ACTION_PREFIX + CACHE + "FOUND_STREAM_EVENT";
        public static final String CACHE_REMOVED_STREAM_EVENT = BASE_ACTION_PREFIX + CACHE + "CACHE_REMOVED_STREAM_EVENT";
    }

    public static class Network {
        private static final String NETWORK = "NETWORK.";

        public static final String STORY_META_DOWNLOADED_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_META_DOWNLOADED_INTENT";
        public static final String STORY_META_DOWNLOADED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_META_DOWNLOADED_EVENT";

        public static final String RANDOM_STORY_META_DOWNLOADED_INTENT = BASE_ACTION_PREFIX + NETWORK + "RANDOM_STORY_META_DOWNLOADED_INTENT";

        public static final String STORY_PAGE_META_DOWNLOADED_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_PAGE_META_DOWNLOADED_INTENT";
        public static final String STORY_PAGE_META_DOWNLOADED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_PAGE_META_DOWNLOADED_EVENT";
        public static final String STORY_IS_LAST_PAGE_DOWNLOADED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_IS_LAST_PAGE_DOWNLOADED_EVENT";
        public static final String STORY_IS_FIRST_PAGE_DOWNLOADED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_IS_FIRST_PAGE_DOWNLOADED_EVENT";

        public static final String STORY_BOOKMARKED_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_BOOKMARKED_INTENT";
        public static final String STORY_BOOKMARKED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_BOOKMARKED_EVENT";

        public static final String STORY_LISTENED_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_LISTENED_INTENT";
        public static final String STORY_LISTENED_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_LISTENED_EVENT";

        public static final String STORY_ADD_RECENT_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_ADD_RECENT_INTENT";
        public static final String STORY_ADD_RECENT_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_ADD_RECENT_EVENT";

        public static final String STORY_REMOVE_RECENT_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_REMOVE_RECENT_INTENT";
        public static final String STORY_REMOVE_RECENT_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_REMOVE_RECENT_EVENT";

        public static final String STORY_DOWNLOAD_RATING_EVENT = BASE_ACTION_PREFIX + NETWORK + "STORY_DOWNLOAD_RATING_EVENT";
        public static final String STORY_RATED_INTENT = BASE_ACTION_PREFIX + NETWORK + "STORY_RATED_INTENT";

        public static final String METRIC_DOWNLOAD_INTENT = BASE_ACTION_PREFIX + NETWORK + "METRIC_DOWNLOAD_INTENT";
        public static final String METRIC_DOWNLOAD_EVENT = BASE_ACTION_PREFIX + NETWORK + "METRIC_DOWNLOAD_EVENT";

        public static final String API_ERROR_EVENT = BASE_ACTION_PREFIX + NETWORK + "API_ERROR_EVENT";
    }

    public static class Ui {
        private static final String UI = "UI.";

        public static final String NEW_STORIES_TYPE_SELECTED_INTENT = BASE_ACTION_PREFIX + UI + "NEW_STORIES_TYPE_SELECTED_INTENT";
        public static final String NEW_STORIES_TYPE_SELECTED_EVENT = BASE_ACTION_PREFIX + UI + "NEW_STORIES_TYPE_SELECTED_EVENT";

        public static final String SAME_STORY_SELECTED_INTENT = BASE_ACTION_PREFIX + UI + "SAME_STORY_SELECTED_INTENT";
        public static final String STORY_COMMENTS_INTENT = BASE_ACTION_PREFIX + UI + "STORY_COMMENTS_INTENT";
        public static final String METRICS_INTENT = BASE_ACTION_PREFIX + UI + "METRICS_INTENT";

        public static final String STORY_BOOKMARKS_CONSIDERED_INTENT = BASE_ACTION_PREFIX + UI + "STORY_BOOKMARKS_CONSIDERED_INTENT";
        public static final String STORY_FROM_LAST_BOOKMARK_PLAYED_EVENT = BASE_ACTION_PREFIX + UI + "STORY_FROM_LAST_BOOKMARK_PLAYED_INTENT";
        public static final String STORY_FROM_START_PLAYED_EVENT = BASE_ACTION_PREFIX + UI + "STORY_FROM_START_PLAYED_EVENT";

        public static final String SCREEN_SWITCHED_EVENT = BASE_ACTION_PREFIX + UI + "SCREEN_SWITCHED_EVENT";
    }

    public static class Application {
        private static final String APPLICATION = "APPLICATION.";

        public static final String USER_LOGIN_INTENT = BASE_ACTION_PREFIX + APPLICATION + "USER_LOGIN_INTENT";
        public static final String USER_LOGIN_EVENT = BASE_ACTION_PREFIX + APPLICATION + "USER_LOGIN_EVENT";
        public static final String USER_LOGIN_CANCELLED_EVENT = BASE_ACTION_PREFIX + APPLICATION + "USER_LOGIN_CANCELLED_EVENT";

        public static final String USER_LOGOUT_INTENT = BASE_ACTION_PREFIX + APPLICATION + "USER_LOGOUT_INTENT";
        public static final String USER_LOGOUT_EVENT = BASE_ACTION_PREFIX + APPLICATION + "USER_LOGOUT_EVENT";

        public static final String USER_REGISTERED_INTENT = BASE_ACTION_PREFIX + APPLICATION + "USER_REGISTERED_INTENT";

        public static final String USER_FORGET_INTENT = BASE_ACTION_PREFIX + APPLICATION + "USER_FORGET_INTENT";

        public static final String STARTED_EVENT = BASE_ACTION_PREFIX + APPLICATION + "STARTED_EVENT";
    }

    public static class Service {
        private static final String SERVICE = "SERVICE.";
        public static final String STARTED_EVENT = BASE_ACTION_PREFIX + SERVICE + "STARTED_EVENT";
    }

    public static class Timer {
        private static final String TIMER = "TIMER.";

        public static final String ENABLED_INTENT = BASE_ACTION_PREFIX + TIMER + "ENABLED_INTENT";
        public static final String ENABLED_EVENT = BASE_ACTION_PREFIX + TIMER + "ENABLED_EVENT";

        public static final String DISABLED_INTENT = BASE_ACTION_PREFIX + TIMER + "DISABLED_INTENT";
        public static final String DISABLED_EVENT = BASE_ACTION_PREFIX + TIMER + "DISABLED_EVENT";

        public static final String FIRED_EVENT = BASE_ACTION_PREFIX + TIMER + "FIRED_EVENT";
    }
}
