package org.grakovne.mds.android.network.common;

import android.content.Context;
import android.util.Log;

import org.grakovne.mds.android.common.ActionNames;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.grakovne.mds.android.MdsApplication.APP_TAG;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.Action;
import static org.grakovne.mds.android.utils.CommonUtils.ifDebugEnabled;

public abstract class QueryResult<T> implements Callback<T> {

    private final Context context;

    public QueryResult(Context context) {
        this.context = context;
    }

    public abstract void onResult(Response<T> response);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            onResult(response);
            return;
        }

        logApiError(call, response);
        getFaultAction().apply();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        send(context, ActionNames.Network.API_ERROR_EVENT);
    }

    public Action getFaultAction() {
        return () -> send(context, ActionNames.Network.API_ERROR_EVENT);
    }

    private void logApiError(Call<T> call, Response response) {
        ifDebugEnabled(() -> Log.d(APP_TAG,
                "API error occurred while request: "
                        + call.request().method()
                        + ", "
                        + call.request().url()
                        + " status code is "
                        + response.code()));
    }
}
