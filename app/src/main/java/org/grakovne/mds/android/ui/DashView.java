package org.grakovne.mds.android.ui;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.common.StoryTab;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.network.domain.responses.Story;

import static org.grakovne.mds.android.common.ActionNames.Network;
import static org.grakovne.mds.android.common.ActionNames.Player;
import static org.grakovne.mds.android.common.ActionNames.Ui;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;

public class DashView extends RecyclerView {

    private Receiver receiver;
    private CacheManager cacheManager;

    public DashView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        cacheManager = CacheManager.getManager(context);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), VERTICAL, false);
        setLayoutManager(linearLayoutManager);
        addOnScrollListener(new StoryDashScrollListener(getContext(), linearLayoutManager));
        setItemAnimator(new DefaultItemAnimator());
    }

    private void subscribe() {

        receiver = new Receiver(getContext(),
                Network.STORY_META_DOWNLOADED_EVENT,
                Ui.NEW_STORIES_TYPE_SELECTED_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Network.STORY_META_DOWNLOADED_EVENT:
                        Story story = getPayloadObject(intent, STORY, Story.class);
                        BroadcastManager.send(getContext(), Player.STARTED_INTENT, story);
                        break;

                    case Ui.NEW_STORIES_TYPE_SELECTED_INTENT:
                        checkSwipes();
                        break;
                }
            }
        };
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        subscribe();
        checkSwipes();
    }

    private void checkSwipes() {
        StoryDashAdapter adapter = (StoryDashAdapter) getAdapter();

        if (null == adapter) {
            return;
        }

        if (cacheManager.findStoryType().equals(StoryTab.RECENT_STORIES)) {
            adapter.setOnSwipedItemAction(getRecentRemoveSwipeAction());
            adapter.enableSwipes();
        } else {
            adapter.disableSwipes();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        receiver.unsubscribe();
        super.onDetachedFromWindow();
    }

    private StoryDashAdapter.OnSwipedItemAction getRecentRemoveSwipeAction() {
        return (story, position) -> {
            StoryDashAdapter adapter = (StoryDashAdapter) getAdapter();
            adapter.removeItem(story, position);
            send(getContext(), Network.STORY_REMOVE_RECENT_INTENT, story);
            Snackbar.make(DashView.this, R.string.recent_remove_snack, Snackbar.LENGTH_LONG)
                    .setActionTextColor(getContext().getResources().getColor(R.color.colorAccent))
                    .setAction(R.string.snackbar_undo, view -> {
                        send(getContext(), Network.STORY_ADD_RECENT_INTENT, story);
                        adapter.addItem(story, position);
                    })
                    .show();
        };
    }
}
