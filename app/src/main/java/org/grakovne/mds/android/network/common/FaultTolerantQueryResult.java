package org.grakovne.mds.android.network.common;

import android.content.Context;

import static org.grakovne.mds.android.utils.CommonUtils.Action;
import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;

public abstract class FaultTolerantQueryResult<T> extends QueryResult<T> {
    public FaultTolerantQueryResult(Context context) {
        super(context);
    }

    @Override
    public Action getFaultAction() {
        return emptyAction();
    }
}
