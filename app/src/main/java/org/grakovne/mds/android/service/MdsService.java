package org.grakovne.mds.android.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.grakovne.mds.android.common.ActionNames;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.DisposableReceiver;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.manager.SettingsManager;

import static org.grakovne.mds.android.MdsApplication.APP_TAG;
import static org.grakovne.mds.android.common.PayloadNames.NOTIFICATION;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.ifDebugEnabled;


public abstract class MdsService extends Service {

    protected static final Integer MDS_NOTIFICATION_IDENTITY = 26_06_96;

    protected SettingsManager settingsManager;
    protected CacheManager cacheManager;

    private Receiver receiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        init();
        subscribe();
        super.onCreate();
    }

    protected void init() {
        settingsManager = SettingsManager.getManager(this);
        cacheManager = CacheManager.getManager(this);
    }

    protected void subscribe() {

        receiver = new Receiver(this,
                Player.PLAYED_EVENT,
                Player.PAUSED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Player.PLAYED_EVENT:
                        foregroundService();
                        break;
                    case Player.PAUSED_EVENT:
                        backgroundService();
                        break;
                }
            }
        };

        send(this, ActionNames.Service.STARTED_EVENT);
    }

    protected void unsubscribe() {
        receiver.unsubscribe();
    }

    protected Context getContext() {
        return this;
    }

    protected void foregroundService() {

        new DisposableReceiver(this, ActionNames.Notification.GOT_EVENT) {
            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case ActionNames.Notification.GOT_EVENT:
                        startForeground(MDS_NOTIFICATION_IDENTITY, getPayloadObject(intent, NOTIFICATION, Notification.class));
                        ifDebugEnabled(() -> Log.d(APP_TAG, getContext().getClass().getSimpleName() + " run in foreground."));
                        break;
                }
            }
        };

        send(this, ActionNames.Notification.GOT_INTENT);
    }

    protected void backgroundService() {
        stopForeground(false);
        ifDebugEnabled(() -> Log.d(APP_TAG, getContext().getClass().getSimpleName() + " run in background."));
    }

}
