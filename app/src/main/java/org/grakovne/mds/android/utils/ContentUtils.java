package org.grakovne.mds.android.utils;

import java.util.Locale;

public class ContentUtils {
    public static String prepareFileBitrate(Long bitrate) {
        return bitrate + " kb/s";
    }

    public static String parseLength(Integer length) {
        int minutes = length / 60;
        int seconds = length % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }

    public static String parseTime(Long length) {
        long hours = length / 3600;
        long minutes = (length % 3600) / 60;
        long seconds = length % 60;
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static int parseRank(Double storyRang) {
        return (int) Math.round(storyRang / 2f);
    }
}
