package org.grakovne.mds.android.network.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.manager.NetworkManager;
import org.grakovne.mds.android.network.common.FaultTolerantQueryResult;
import org.grakovne.mds.android.network.common.QueryResult;
import org.grakovne.mds.android.network.domain.ApiResponse;
import org.grakovne.mds.android.network.domain.forms.UserForm;
import org.grakovne.mds.android.network.domain.responses.Metric;
import org.grakovne.mds.android.network.domain.responses.User;
import org.grakovne.mds.android.network.repository.UserRepository;
import org.grakovne.mds.android.service.MdsService;
import org.grakovne.mds.android.utils.AccountUtils;

import okhttp3.OkHttpClient;
import retrofit2.Response;

import static org.grakovne.mds.android.common.PayloadNames.USER_FORM;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;

@SuppressWarnings("ConstantConditions")
public class UserService extends MdsService {
    private UserRepository userRepository;
    private CacheManager cacheManager;

    private Receiver receiver;

    @Override
    protected void init() {
        OkHttpClient client = NetworkManager.getManager(this).getApiClient();
        userRepository = UserRepository.ApiFactory.create(client);
        cacheManager = CacheManager.getManager(this);
    }

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                Application.USER_REGISTERED_INTENT,
                Application.USER_LOGOUT_INTENT,
                Application.USER_LOGIN_EVENT,
                Application.USER_LOGOUT_EVENT,
                Application.USER_FORGET_INTENT,
                Network.METRIC_DOWNLOAD_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Application.USER_REGISTERED_INTENT:
                        UserForm form = getPayloadObject(intent, USER_FORM, UserForm.class);
                        registerUser(form);
                        break;

                    case Application.USER_LOGOUT_INTENT:
                        logoutUser();
                        break;

                    case Application.USER_LOGIN_EVENT:
                    case Application.USER_LOGOUT_EVENT:
                        init();
                        break;

                    case Network.METRIC_DOWNLOAD_INTENT:
                        ifUserLogged(getContext(), () -> {
                            downloadMetrics();
                        });
                        break;

                    case Application.USER_FORGET_INTENT:
                        ifUserLogged(getContext(), () -> {
                            forgetUser();
                        });
                        break;
                }
            }
        };
    }

    private void downloadMetrics() {
        userRepository.getUserMetrics().enqueue(new QueryResult<ApiResponse<Metric>>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse<Metric>> response) {
                BroadcastManager.send(getContext(), Network.METRIC_DOWNLOAD_EVENT, response.body().getBody());
            }
        });
    }

    private void logoutUser() {
        cacheManager.setUserName(null);
        cacheManager.setUserEmail(null);
        cacheManager.setUserImageUrl(null);
        cacheManager.setUserAuthToken(null);

        send(this, Application.USER_LOGOUT_EVENT);
    }

    private void registerUser(final UserForm form) {
        userRepository.registerUser(form).enqueue(new QueryResult<ApiResponse<User>>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse<User>> response) {
                User user = response.body().getBody();

                String authToken = AccountUtils.getAuthToken(form);
                cacheManager.setUserAuthToken(authToken);

                send(getContext(), Application.USER_LOGIN_EVENT, user);
            }
        });
    }

    private void forgetUser() {
        userRepository.forgetUser().enqueue(new FaultTolerantQueryResult<ApiResponse>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse> response) {
                BroadcastManager.send(getContext(), Application.USER_LOGOUT_INTENT);
            }
        });
    }

    @Override
    public void unsubscribe() {
        receiver.unsubscribe();
    }
}
