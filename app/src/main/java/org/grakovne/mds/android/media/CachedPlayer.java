package org.grakovne.mds.android.media;

import android.content.Context;

import org.grakovne.mds.android.utils.CacheUtils;

public class CachedPlayer extends StoryPlayer {

    public CachedPlayer(Context context, DataSource dataSource) {
        super(context, dataSource);
    }

    @Override
    public String toUri(DataSource source) {
        return CacheUtils.buildStreamFile(context, source.getStory()).getAbsolutePath();
    }

    @Override
    public void onConfigured(DataSource source) {
        dataSource = new DataSource(source.getStory(), 0);
        mediaPlayer.seekTo(source.getOffset() * 1000);
    }
}
