package org.grakovne.mds.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.session.MediaSession;
import android.support.annotation.NonNull;
import android.view.KeyEvent;

import org.grakovne.mds.android.MdsApplication;
import org.grakovne.mds.android.common.ActionNames;

import static android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY;
import static android.view.KeyEvent.ACTION_UP;
import static android.view.KeyEvent.KEYCODE_MEDIA_PAUSE;
import static android.view.KeyEvent.KEYCODE_MEDIA_PLAY;
import static org.grakovne.mds.android.manager.BroadcastManager.send;

public class MediaButtonService extends MdsService {

    private BroadcastReceiver headphonesReceiver;
    private MediaSession mediaSession;

    @Override
    protected void init() {
        super.init();

        mediaSession = new MediaSession(this, MdsApplication.APP_TAG);
        mediaSession.setCallback(new MediaSession.Callback() {
            @Override
            public boolean onMediaButtonEvent(@NonNull final Intent intent) {
                handleMediaButtons(intent);
                return true;
            }
        });

        mediaSession.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setActive(true);
    }

    @Override
    protected void subscribe() {
        super.subscribe();

        headphonesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                    send(getContext(), ActionNames.Player.PAUSED_INTENT);
                }
            }
        };

        registerReceiver(headphonesReceiver, new IntentFilter(ACTION_AUDIO_BECOMING_NOISY));
    }

    @Override
    protected void unsubscribe() {
        unregisterReceiver(headphonesReceiver);
        mediaSession.release();
        super.unsubscribe();
    }

    private void handleMediaButtons(Intent intent) {
        KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

        if (null == event
                || event.getAction() != ACTION_UP
                || null == cacheManager.getStory()) {

            return;
        }

        switch (event.getKeyCode()) {
            case KEYCODE_MEDIA_PAUSE:
            case KEYCODE_MEDIA_PLAY:
                send(
                        this,
                        settingsManager.isStoryPlaying()
                                ? ActionNames.Player.PAUSED_INTENT
                                : ActionNames.Player.PLAYED_INTENT,
                        cacheManager.getStory()
                );
                break;
        }
    }
}
