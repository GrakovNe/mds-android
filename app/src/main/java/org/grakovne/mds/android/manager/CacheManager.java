package org.grakovne.mds.android.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;

import org.grakovne.mds.android.common.StoryTab;
import org.grakovne.mds.android.network.domain.forms.RatingForm;
import org.grakovne.mds.android.network.domain.responses.Story;

import static android.content.Context.MODE_PRIVATE;
import static org.grakovne.mds.android.common.SearchParams.ORDER_ASC;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_INTERACTIVE;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_RATING;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_TITLE;

public class CacheManager extends DataManager {

    private static final String BASE_ACTION_PREFIX = "org.grakovne.mds.android.cache";
    private static final String APPLICATION_PREFERENCES = BASE_ACTION_PREFIX + "application_cache";
    private static final String CACHE_STORY = BASE_ACTION_PREFIX + "cache_playing_story";
    private static final String CACHE_RATING = BASE_ACTION_PREFIX + "cache_playing_rating";
    private static final String SEARCH_STORY_QUERY = BASE_ACTION_PREFIX + "search_story_query";
    private static final String SEARCH_STORY_ORDER_DIRECTION = BASE_ACTION_PREFIX + "search_story_order_direction";
    private static final String SEARCH_STORY_ORDER_FIELD = BASE_ACTION_PREFIX + "search_story_order_field";
    private static final String SEARCH_STORY_PAGE_NUMBER = BASE_ACTION_PREFIX + "search_story_page_number";
    private static final String SEARCH_STORY_TYPE = BASE_ACTION_PREFIX + "search_story_type";
    private static final String AUTH_USER_TOKEN = BASE_ACTION_PREFIX + "auth_user_token";
    private static final String AUTH_USER_NAME = BASE_ACTION_PREFIX + "auth_user_name";
    private static final String AUTH_USER_EMAIL = BASE_ACTION_PREFIX + "auth_user_email";
    private static final String AUTH_USER_IMAGE_URL = BASE_ACTION_PREFIX + "auth_user_image_url";
    private static CacheManager instance;

    private CacheManager(Context context) {
        sharedPreferences = context.getSharedPreferences(APPLICATION_PREFERENCES, MODE_PRIVATE);

        setStory(null);
        setRating(null);
        setSearchPageNumber(null);
        setSearchDirection(null);
        setSearchOrderField(null);
        setSearchQuery(null);
    }

    public static CacheManager getManager(Context context) {

        if (null == instance) {
            instance = new CacheManager(context);
        }

        return instance;
    }

    public Story getStory() {
        String serialized = sharedPreferences.getString(CACHE_STORY, "");

        if (TextUtils.isEmpty(serialized)) {
            return null;
        }

        return gson.fromJson(serialized, Story.class);
    }

    public void setStory(Story story) {
        String serialized = null == story ? "" : gson.toJson(story, Story.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CACHE_STORY, serialized);
        editor.apply();
    }

    public RatingForm getRating() {
        String serializedStory = sharedPreferences.getString(CACHE_RATING, "");

        if (TextUtils.isEmpty(serializedStory)) {
            return null;
        }

        return gson.fromJson(serializedStory, RatingForm.class);
    }

    public void setRating(RatingForm ratingForm) {
        String serialized = null == ratingForm ? "" : gson.toJson(ratingForm, RatingForm.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CACHE_RATING, serialized);
        editor.apply();
    }

    public String getSearchQuery() {
        return sharedPreferences.getString(SEARCH_STORY_QUERY, "");
    }

    public void setSearchQuery(String storyTitle) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, SEARCH_STORY_QUERY, storyTitle);
        editor.apply();
    }

    public String getSearchDirection() {
        return sharedPreferences.getString(SEARCH_STORY_ORDER_DIRECTION, ORDER_ASC);
    }

    public void setSearchDirection(String storyOrderDirection) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, SEARCH_STORY_ORDER_DIRECTION, storyOrderDirection, ORDER_ASC);
        editor.apply();
    }

    public String getSearchOrderField() {
        return sharedPreferences.getString(SEARCH_STORY_ORDER_FIELD, SEARCH_TITLE);
    }

    public void setSearchOrderField(String storyOrderField) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, SEARCH_STORY_ORDER_FIELD, storyOrderField, SEARCH_TITLE);
        editor.apply();
    }

    public Integer getSearchPageNumber() {
        return sharedPreferences.getInt(SEARCH_STORY_PAGE_NUMBER, 0);
    }

    public void setSearchPageNumber(Integer pageNumber) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putInteger(editor, SEARCH_STORY_PAGE_NUMBER, pageNumber);
        editor.apply();
    }

    public String getSearchType() {
        return sharedPreferences.getString(SEARCH_STORY_TYPE, SEARCH_INTERACTIVE);
    }

    public void setSearchType(String storyType) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, SEARCH_STORY_TYPE, storyType, SEARCH_INTERACTIVE);
        editor.apply();
    }

    public String findStoryType() {
        if (getSearchType().equals(StoryTab.RECENT_STORIES)) {
            return StoryTab.RECENT_STORIES;
        }

        if (getSearchType().equals(SEARCH_INTERACTIVE) && getSearchOrderField().equals(SEARCH_RATING)) {
            return StoryTab.BEST_STORIES;
        }

        return StoryTab.ALL_STORIES;
    }

    public String getUserAuthToken() {
        return sharedPreferences.getString(AUTH_USER_TOKEN, "");
    }

    public void setUserAuthToken(String authToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, AUTH_USER_TOKEN, authToken);
        editor.apply();
    }

    public Boolean isUserLogged() {
        return !TextUtils.isEmpty(getUserAuthToken());
    }

    public String getUserName() {
        return sharedPreferences.getString(AUTH_USER_NAME, "");
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, AUTH_USER_NAME, userName);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(AUTH_USER_EMAIL, "");
    }

    public void setUserEmail(String userEmail) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, AUTH_USER_EMAIL, userEmail);
        editor.apply();
    }

    public Uri getUserImageUrl() {
        return Uri.parse(sharedPreferences.getString(AUTH_USER_IMAGE_URL, ""));
    }

    public void setUserImageUrl(Uri url) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        putString(editor, AUTH_USER_IMAGE_URL, String.valueOf(url));
        editor.apply();
    }

}
