package org.grakovne.mds.android.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.media.TimerSettings;

import java.util.Timer;
import java.util.TimerTask;

import static org.grakovne.mds.android.common.ActionNames.Timer.DISABLED_EVENT;
import static org.grakovne.mds.android.common.ActionNames.Timer.DISABLED_INTENT;
import static org.grakovne.mds.android.common.ActionNames.Timer.ENABLED_EVENT;
import static org.grakovne.mds.android.common.ActionNames.Timer.ENABLED_INTENT;
import static org.grakovne.mds.android.common.ActionNames.Timer.FIRED_EVENT;
import static org.grakovne.mds.android.common.PayloadNames.TIMER_SETTING;

public class TimerService extends MdsService {

    private Receiver receiver;
    private Timer pauseTimer;

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                DISABLED_INTENT,
                ENABLED_INTENT,
                FIRED_EVENT,
                Player.PAUSED_EVENT,
                Player.STARTED_EVENT,
                Player.FINISHED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case DISABLED_INTENT:
                    case Player.STARTED_EVENT:
                    case Player.FINISHED_EVENT:
                    case Player.PAUSED_EVENT:
                        disableTimer();
                        break;

                    case ENABLED_INTENT:
                        enableTimer(BroadcastManager.getPayloadObject(intent, TIMER_SETTING, TimerSettings.class));
                        playStory();
                        break;

                    case FIRED_EVENT:
                        pauseStory();
                        break;
                }
            }
        };
    }

    private void playStory() {
        BroadcastManager.send(this, Player.PLAYED_INTENT);
    }

    private void pauseStory() {
        BroadcastManager.send(this, Player.PAUSED_INTENT);
        disableTimer();
    }

    private void disableTimer() {

        if (null != pauseTimer) {
            pauseTimer.cancel();
        }

        settingsManager.setTimerEnabled(false);
        BroadcastManager.send(this, DISABLED_EVENT);
    }

    private void enableTimer(TimerSettings settings) {
        disableTimer();
        pauseTimer = new Timer();
        pauseTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                BroadcastManager.send(getContext(), FIRED_EVENT);
            }
        }, settings.getMilliseconds());

        settingsManager.setTimerEnabled(true);
        BroadcastManager.send(this, ENABLED_EVENT);
    }

    @Override
    protected void unsubscribe() {
        receiver.unsubscribe();
        super.unsubscribe();
    }
}
