package org.grakovne.mds.android.common;

public class SearchParams {
    public static final String UNLISTENED = "unlistened";
    public static final String BOTH = "both";

    public static final String ORDER_ASC = "asc";
    public static final String ORDER_DESC = "desc";

    public static final String SEARCH_INTERACTIVE = "";

    public static final String SEARCH_TITLE = "title";
    public static final String SEARCH_RATING = "rating";
}
