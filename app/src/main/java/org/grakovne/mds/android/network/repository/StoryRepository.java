package org.grakovne.mds.android.network.repository;

import org.grakovne.mds.android.network.common.ApiNames;
import org.grakovne.mds.android.network.domain.ApiResponse;
import org.grakovne.mds.android.network.domain.PageContent;
import org.grakovne.mds.android.network.domain.forms.BookmarkForm;
import org.grakovne.mds.android.network.domain.forms.RatingForm;
import org.grakovne.mds.android.network.domain.responses.Story;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface StoryRepository {
    @GET("story/{id}")
    Call<ApiResponse<Story>> getStory(@Path("id") Integer id);

    @GET("story/random")
    Call<ApiResponse<Story>> getRandomStory();

    @GET()
    Call<ApiResponse<PageContent<List<Story>>>> getStoryPage(@Url String url);

    @POST("story/{id}/listen")
    Call<ApiResponse> makeStoryAsListened(@Path("id") Integer id);

    @POST("story/{id}/recent")
    Call<ApiResponse> addRecent(@Path("id") Integer id);

    @DELETE("story/{id}/recent")
    Call<ApiResponse> removeRecent(@Path("id") Integer id);

    @POST("story/{id}/rating")
    Call<ApiResponse<Story>> rateStory(@Path("id") Integer id, @Body RatingForm form);

    @POST("story/{id}/bookmark")
    Call<Void> bookmarkStory(@Path("id") Integer id, @Body BookmarkForm form);

    class ApiFactory {
        public static StoryRepository create(OkHttpClient okHttpClient) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ApiNames.getApiUrl())
                    .client(okHttpClient)
                    .build();

            return retrofit.create(StoryRepository.class);
        }
    }
}
