package org.grakovne.mds.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.network.domain.forms.RatingForm;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.utils.ContentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;

public class RatingView extends LinearLayout {

    private final Context context;
    @BindView(R.id.rank_container)
    LinearLayout rankContainer;

    private Integer minimalRating;
    private Integer maximalRating;
    private CacheManager cacheManager;

    public RatingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray attributes = context
                .getTheme()
                .obtainStyledAttributes(
                        attrs,
                        R.styleable.RatingView,
                        0, 0);

        cacheManager = CacheManager.getManager(getContext());
        minimalRating = attributes.getInteger(R.styleable.RatingView_minimalRating, 0);
        maximalRating = attributes.getInteger(R.styleable.RatingView_maximalRating, 5);

        LayoutInflater.from(context).inflate(R.layout.rating_view, this, true);
        ButterKnife.bind(this);
        drawStars(maximalRating, minimalRating);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void drawRating(Story story) {
        if (null != story.getRating()) {
            drawRating(story.getRating());
            return;
        }

        drawStars(maximalRating, minimalRating);
    }

    private void drawRating(Double value) {
        Integer fullStarsNum = Math.round(ContentUtils.parseRank(value));
        Integer emptyStarsNum = maximalRating - fullStarsNum;

        drawStars(emptyStarsNum, fullStarsNum);
    }

    private void drawStars(Integer emptyStarsNum, Integer fullStarsNum) {
        rankContainer.removeAllViews();

        for (int i = 0; i < fullStarsNum; i++) {
            rankContainer.addView(getImageView(R.mipmap.rating_full_icon));
        }

        for (int i = 0; i < emptyStarsNum; i++) {
            rankContainer.addView(getImageView(R.mipmap.rating_empty_icon));
        }

        for (int i = 0; i < rankContainer.getChildCount(); i++) {
            Double ratingValue = 2.0 * (i + 1);
            rankContainer.getChildAt(i).setOnClickListener(v -> sendRateStoryIntent(ratingValue));
        }
    }

    private ImageView getImageView(Integer imageResource) {
        ImageView result = new ImageView(context);
        result.setImageResource(imageResource);
        return result;
    }

    private void sendRateStoryIntent(Double value) {
        ifUserLogged(context, () -> {
            if (null != value && null != cacheManager.getStory()) {
                BroadcastManager.send(getContext(),
                        Network.STORY_RATED_INTENT,
                        new RatingForm(cacheManager.getStory().getId(), value)
                );

                drawRating(value);
            }
        });
    }
}
