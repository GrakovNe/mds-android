package org.grakovne.mds.android.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Notification;
import org.grakovne.mds.android.common.ExternalReceiver;
import org.grakovne.mds.android.common.Receiver;

import static org.grakovne.mds.android.common.ActionNames.Player;
import static org.grakovne.mds.android.manager.BroadcastManager.send;

public class GateService extends MdsService {

    private Receiver receiver;

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new ExternalReceiver(this,
                Player.PLAYED_INTENT,
                Player.PAUSED_INTENT,
                Notification.PRESSED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                send(getContext(), action);
            }
        };

    }

    @Override
    protected void unsubscribe() {
        receiver.unsubscribe();
        super.unsubscribe();
    }
}
