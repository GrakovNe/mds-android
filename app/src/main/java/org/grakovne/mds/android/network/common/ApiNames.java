package org.grakovne.mds.android.network.common;

import org.grakovne.mds.android.manager.SettingsManager;

public class ApiNames {

    private final static String API_BASE_URL_PRODUCTION = "https://mds.grakovne.org/api/v1/";
    private final static String API_BASE_URL_DEVELOP = "https://mds-dev.grakovne.org/api/v1/";
    private final static String GRAKOVNE_BLOG_URL = "https://grakovne.org";
    private final static String VK_PUBLIC_URL = "vkontakte://public/assembly_model";


    public static String getApiUrl() {
        return SettingsManager.isDebugEnabled() ? API_BASE_URL_DEVELOP : API_BASE_URL_PRODUCTION;
    }

    public static String getGrakovneBlogUrl() {
        return GRAKOVNE_BLOG_URL;
    }

    public static String getVkPublicUrl() {
        return VK_PUBLIC_URL;
    }
}
