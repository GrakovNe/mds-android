package org.grakovne.mds.android.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.utils.CacheUtils;
import org.grakovne.mds.android.utils.ContentUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static android.support.v7.widget.helper.ItemTouchHelper.LEFT;
import static android.support.v7.widget.helper.ItemTouchHelper.RIGHT;
import static android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.ifSameStory;

public class StoryDashAdapter extends RecyclerView.Adapter<StoryDashAdapter.StoryViewHolder> {
    private List<Story> content;
    private Boolean isDirtyContent;
    private ItemTouchHelper swipeListener;
    private RecyclerView dashView;
    private OnSwipedItemAction onSwipedItemAction;

    public StoryDashAdapter(RecyclerView dashView) {
        this.dashView = dashView;
        isDirtyContent = true;
        content = new ArrayList<>();

        swipeListener = new ItemTouchHelper(new SimpleCallback(0, LEFT | RIGHT) {
            @Override
            public boolean onMove(RecyclerView view, ViewHolder holder, ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(ViewHolder holder, int direction) {
                Story story = ((StoryViewHolder) holder).story;
                int position = holder.getAdapterPosition();
                if (null != onSwipedItemAction) {
                    onSwipedItemAction.onSwiped(story, position);
                }
            }
        });
    }

    public void setOnSwipedItemAction(OnSwipedItemAction onSwipedItemAction) {
        this.onSwipedItemAction = onSwipedItemAction;
    }

    public void enableSwipes() {
        swipeListener.attachToRecyclerView(dashView);
    }

    public void disableSwipes() {
        swipeListener.attachToRecyclerView(null);
    }

    public void setContent(Collection<Story> newContent) {
        this.content = new ArrayList<>(newContent);
        notifyDataSetChanged();
    }

    public void removeItem(Story story, int position) {
        content.remove(story);
        notifyItemRemoved(position);
    }

    public void addItem(Story story, int position) {
        content.add(position, story);
        notifyItemInserted(position);
        notifyDataSetChanged();
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public void addContent(Collection<Story> content) {
        if (null == this.content) {
            this.content = new ArrayList<>(content.size());
        }

        if (isDirtyContent) {
            this.content.clear();
            isDirtyContent = false;
        }

        this.content.addAll(content);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.dash_story_item, parent, false);
        return new StoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryDashAdapter.StoryViewHolder holder, int position) {
        Story story = content.get(position);

        holder.playNowButton.setImageResource(CacheUtils.isStreamCached(dashView.getContext(), story)
                ? R.mipmap.dash_offline_story_icon
                : R.mipmap.dash_play_icon);

        holder.title.setText(story.getTitle());

        if (null != story.getAuthors() && story.getAuthors().isEmpty()) {
            holder.authors.setVisibility(View.GONE);
        } else {
            holder.authors.setVisibility(View.VISIBLE);
            holder.authors.setText(story.getAuthors());
        }

        holder.fileDuration.setText(ContentUtils.parseLength(story.getLength()));
        holder.bitrate.setText(ContentUtils.prepareFileBitrate(story.getFileQuality()));

        holder.story = story;
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public void clearContent() {
        isDirtyContent = true;
    }

    public interface OnSwipedItemAction {
        void onSwiped(Story story, Integer position);
    }

    protected class StoryViewHolder extends ViewHolder {

        private final ImageView playNowButton;
        private final TextView title;
        private final TextView authors;
        private final TextView fileDuration;
        private final TextView bitrate;

        private Story story = null;

        private StoryViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            authors = itemView.findViewById(R.id.authors);
            playNowButton = itemView.findViewById(R.id.story_playing_image);

            fileDuration = itemView.findViewById(R.id.duration);
            bitrate = itemView.findViewById(R.id.bitrate);

            itemView.setOnClickListener(view ->
                    ifSameStory(itemView.getContext(), story,
                            () -> send(view.getContext(),
                                    Ui.SAME_STORY_SELECTED_INTENT),
                            () -> {
                                send(view.getContext(), Player.PAUSED_INTENT);
                                send(view.getContext(), Network.STORY_META_DOWNLOADED_INTENT, story);
                            }));
        }
    }
}
