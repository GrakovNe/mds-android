package org.grakovne.mds.android;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;

import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;
import static org.grakovne.mds.android.utils.CommonUtils.ifDebugEnabled;

public class MdsApplication extends Application {

    public final static String APP_TAG = "mds_android";

    @Override
    public void onCreate() {
        super.onCreate();

        ifDebugEnabled(emptyAction(), () -> Fabric.with(this, new Crashlytics()));
        initNavigationBar();
    }

    private void initNavigationBar() {
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder, String tag) {
                Picasso
                        .with(imageView.getContext())
                        .load(uri)
                        .placeholder(R.drawable.user_image)
                        .into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }
        });
    }
}
