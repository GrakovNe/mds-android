package org.grakovne.mds.android.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.DisposableReceiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.network.domain.responses.Story;

import static org.grakovne.mds.android.manager.SettingsManager.isDebugEnabled;

public class CommonUtils {

    public static boolean isVkAppInstalled(Context context) {
        return isAppInstalled(context, "com.vkontakte.android");
    }

    public static void ifDebugEnabled(Action action) {
        ifDebugEnabled(action, emptyAction());
    }

    public static void ifDebugEnabled(Action positive, Action negative) {
        if (isDebugEnabled()) {
            positive.apply();
        } else {
            negative.apply();
        }
    }

    public static void ifSameStory(Context context, Story story, Action positive, Action negative) {
        CacheManager cacheManager = CacheManager.getManager(context);

        Boolean isSame = null != cacheManager.getStory()
                && null != story
                && story.getId().equals(cacheManager.getStory().getId());

        pickAction(isSame, positive, negative);
    }

    public static void ifUserLogged(Context context, Action positive, Action negative) {
        pickAction(CacheManager.getManager(context).isUserLogged(), positive, negative);
    }

    private static void pickAction(boolean condition, Action positive, Action negative) {
        if (condition) {
            positive.apply();
        } else {
            negative.apply();
        }
    }

    public static void ifUserLogged(Context context, Action action) {
        ifUserLogged(context, action, () -> {
            new DisposableReceiver(context,
                    Application.USER_LOGIN_EVENT,
                    Application.USER_LOGIN_CANCELLED_EVENT) {

                @Override
                public void onAction(Intent intent, String a) {
                    switch (a) {
                        case Application.USER_LOGIN_EVENT:
                            action.apply();
                            break;
                    }
                }
            };

            Toast.makeText(context, R.string.sign_in_reqiured_toast, Toast.LENGTH_LONG).show();
            BroadcastManager.send(context, Application.USER_LOGIN_INTENT);
        });
    }

    public static <T> void ifNotNull(T value, Action action) {
        if (null != value) {
            action.apply();
        }
    }

    public static Action emptyAction() {
        return () -> {

        };
    }

    private static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public interface Action {
        void apply();
    }
}
