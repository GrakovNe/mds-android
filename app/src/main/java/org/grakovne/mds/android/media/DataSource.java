package org.grakovne.mds.android.media;

import org.grakovne.mds.android.network.domain.responses.Story;

public class DataSource {
    private final Story story;
    private final Integer offset;

    public DataSource(Story story, Integer offset) {
        this.story = story;
        this.offset = offset;
    }

    public Story getStory() {
        return story;
    }

    public Integer getOffset() {
        return offset;
    }

}
