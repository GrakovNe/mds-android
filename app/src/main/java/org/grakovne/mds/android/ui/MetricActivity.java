package org.grakovne.mds.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.network.domain.responses.Metric;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.grakovne.mds.android.common.ActionNames.Application;
import static org.grakovne.mds.android.common.PayloadNames.METRICS;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.ifNotNull;
import static org.grakovne.mds.android.utils.ContentUtils.parseTime;

public class MetricActivity extends MdsActivity {

    @BindView(R.id.metric_comments)
    TextView metricComments;
    @BindView(R.id.metric_finished_storied)
    TextView metricFinishedStoried;
    @BindView(R.id.metric_rated_storied)
    TextView metricRatedStoried;
    @BindView(R.id.metric_listened_time)
    TextView metricListenedTime;
    @BindView(R.id.metrics_placeholder)
    TextView metricPlaceholder;
    @BindView(R.id.metric_forget_me)
    Button forgetMeButton;
    private Metric metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metric);

        ButterKnife.bind(this);

        initReceivers();
        initViews();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        askMetrics();
    }

    private void askMetrics() {
        if (null == metrics) {
            metricPlaceholder.setText(R.string.fetching_metrics_placeholder);
            metricPlaceholder.setVisibility(VISIBLE);
        }
        send(this, Network.METRIC_DOWNLOAD_INTENT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        toolbar.setTitle(R.string.metrics_title);
        return true;
    }

    @Override
    protected void showApiErrorMessage() {
        metricPlaceholder.setVisibility(VISIBLE);
        metricPlaceholder.setText(R.string.server_error_message);
        super.showApiErrorMessage();
    }

    private void initViews() {
        setSupportActionBar(findViewById(R.id.metrics_action_toolbar));

        forgetMeButton.setOnClickListener(v -> new ConfirmDialog(MetricActivity.this,
                getString(R.string.forget_user_dialog_tittle),
                getString(R.string.forget_user_dialog_text),
                () -> send(this, Application.USER_FORGET_INTENT))
        );
    }

    private void initReceivers() {
        new Receiver(this,
                Network.METRIC_DOWNLOAD_EVENT,
                Network.API_ERROR_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Network.METRIC_DOWNLOAD_EVENT:
                        showMetrics(getPayloadObject(intent, METRICS, Metric.class));
                        break;

                    case Network.API_ERROR_EVENT:
                        showApiErrorMessage();
                        break;
                }
            }
        };
    }

    private void showMetrics(Metric metric) {
        this.metrics = metric;
        ifNotNull(metric.getComments(), () ->
                metricComments.setText(drawMetricElement(getString(R.string.metric_comment_hint), getString(R.string.comments_are_not_implemented))));
        ifNotNull(metric.getRatedStories(), () ->
                metricRatedStoried.setText(drawMetricElement(getString(R.string.metric_rated_stories_hint), metric.getRatedStories().toString())));
        ifNotNull(metric.getListenedTime(), () ->
                metricListenedTime.setText(drawMetricElement(getString(R.string.metric_listened_time_hint), parseTime(metric.getListenedTime()))));
        ifNotNull(metric.getFinishedStories(), () ->
                metricFinishedStoried.setText(drawMetricElement(getString(R.string.metric_stories_hint), metric.getFinishedStories() + " / " + metric.getStories())));
        metricPlaceholder.setVisibility(GONE);
    }

    private SpannableStringBuilder drawMetricElement(String hint, String value) {
        String text = hint + " " + value;
        SpannableStringBuilder builder = new SpannableStringBuilder(text.toUpperCase());
        builder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, hint.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder;
    }

}
