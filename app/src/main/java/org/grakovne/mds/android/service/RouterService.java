package org.grakovne.mds.android.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Notification;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.ui.CommentsActivity;
import org.grakovne.mds.android.ui.DashActivity;
import org.grakovne.mds.android.ui.MetricActivity;
import org.grakovne.mds.android.ui.StoryActivity;

import static org.grakovne.mds.android.common.ActionNames.Player;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;

public class RouterService extends MdsService {

    private Receiver receiver;

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                Player.STARTED_INTENT,
                Ui.NEW_STORIES_TYPE_SELECTED_INTENT,
                Ui.SAME_STORY_SELECTED_INTENT,
                Ui.METRICS_INTENT,
                Notification.PRESSED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                Story story = getPayloadObject(intent, STORY, Story.class);
                if (null == story) {
                    story = cacheManager.getStory();
                }
                switch (action) {
                    case Player.STARTED_INTENT:
                    case Ui.SAME_STORY_SELECTED_INTENT:
                    case Notification.PRESSED_EVENT:
                        startStoryActivity(story);
                        break;

                    case Ui.NEW_STORIES_TYPE_SELECTED_INTENT:
                        startDashActivity();
                        break;

                    case Ui.STORY_COMMENTS_INTENT:
                        startCommentsActivity(story);
                        break;

                    case Ui.METRICS_INTENT:
                        startMetricsActivity();
                }
            }
        };
    }


    private void startMetricsActivity() {
        Intent intent = new Intent(this, MetricActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startDashActivity() {
        Intent intent = new Intent(this, DashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
    }

    private void startStoryActivity(Story story) {
        Intent intent = new Intent(this, StoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(STORY, story);
        this.startActivity(intent);
    }

    private void startCommentsActivity(Story story) {
        Intent intent = new Intent(this, CommentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(STORY, story);
        this.startActivity(intent);
    }

    @Override
    public void unsubscribe() {
        receiver.unsubscribe();
        super.unsubscribe();
    }
}
