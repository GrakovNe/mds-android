package org.grakovne.mds.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.CheckBox;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.enums.BookmarksConsidering;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.SettingsManager;

public class BookmarksDialog {
    private final Context context;
    private final View rememberCheckBoxView;
    private final CheckBox rememberCheckbox;
    private final SettingsManager settingsManager;


    public BookmarksDialog(Activity activity) {
        this.context = activity;
        rememberCheckBoxView = View.inflate(activity, R.layout.is_bookmarks_considered_dialog, null);
        rememberCheckbox = rememberCheckBoxView.findViewById(R.id.story_has_bookmarks_remember_choice);
        settingsManager = SettingsManager.getManager(activity);

        if (!activity.isFinishing()) {
            show();
        }
    }

    private void playFromStart(DialogInterface dialog, int which) {
        if (rememberCheckbox.isChecked()) {
            settingsManager.setBookmarksConsideringType(BookmarksConsidering.NEVER);
        }
        BroadcastManager.send(context, Ui.STORY_FROM_START_PLAYED_EVENT);
    }

    private void playFromBookmark(DialogInterface dialog, int which) {
        if (rememberCheckbox.isChecked()) {
            settingsManager.setBookmarksConsideringType(BookmarksConsidering.ALWAYS);
        }
        BroadcastManager.send(context, Ui.STORY_FROM_LAST_BOOKMARK_PLAYED_EVENT);
    }

    private void show() {
        new AlertDialog.Builder(context)
                .setTitle(R.string.has_bookmarks_dialog_title)
                .setMessage(context.getString(R.string.has_bookmarks_dialog_text))
                .setPositiveButton(R.string.has_bookmarks_dialog_yes, this::playFromBookmark)
                .setNegativeButton(R.string.has_bookmarks_dialog_no, this::playFromStart)
                .setView(rememberCheckBoxView)
                .setCancelable(false)
                .create()
                .show();
    }
}
