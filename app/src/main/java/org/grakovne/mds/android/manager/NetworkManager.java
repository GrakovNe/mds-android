package org.grakovne.mds.android.manager;

import android.content.Context;

import java.util.Collections;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.TlsVersion;

public class NetworkManager {

    private final static String AUTH_COOKIE_HEADER = "Authorization";
    private static NetworkManager instance;
    private final CacheManager cacheManager;

    private NetworkManager(Context context) {
        this.cacheManager = CacheManager.getManager(context);
    }

    public static NetworkManager getManager(Context context) {
        if (null == instance) {
            instance = new NetworkManager(context);
        }

        return instance;
    }

    public OkHttpClient getApiClient() {

        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();

        return new OkHttpClient()
                .newBuilder()
                .addInterceptor(chain -> {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.header(AUTH_COOKIE_HEADER, cacheManager.getUserAuthToken());
                    return chain.proceed(builder.build());
                })
                .connectionSpecs(Collections.singletonList(spec))
                .build();
    }
}
