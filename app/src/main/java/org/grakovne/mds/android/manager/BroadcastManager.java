package org.grakovne.mds.android.manager;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import org.grakovne.mds.android.common.PayloadNames;
import org.grakovne.mds.android.media.PlayingState;
import org.grakovne.mds.android.media.TimerSettings;
import org.grakovne.mds.android.network.domain.forms.RatingForm;
import org.grakovne.mds.android.network.domain.forms.UserForm;
import org.grakovne.mds.android.network.domain.responses.Metric;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.network.domain.responses.User;
import org.grakovne.mds.android.ui.MdsActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.grakovne.mds.android.common.PayloadNames.METRICS;
import static org.grakovne.mds.android.common.PayloadNames.NOTIFICATION;
import static org.grakovne.mds.android.common.PayloadNames.PLAYING_STATE;
import static org.grakovne.mds.android.common.PayloadNames.SCREEN;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.common.PayloadNames.STORY_IS_EDGE_PAGE;
import static org.grakovne.mds.android.common.PayloadNames.STORY_PAGE;
import static org.grakovne.mds.android.common.PayloadNames.STREAM;
import static org.grakovne.mds.android.common.PayloadNames.TIMER_SETTING;
import static org.grakovne.mds.android.common.PayloadNames.USER;
import static org.grakovne.mds.android.common.PayloadNames.USER_FORM;

public class BroadcastManager {

    public static void send(Context context, String actionName, Story story) {
        Intent intent = new Intent(actionName);
        intent.putExtra(STORY, story);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, MdsActivity activity) {
        Intent intent = new Intent(actionName);
        intent.putExtra(SCREEN, activity);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, Story story, PlayingState state) {
        Intent intent = new Intent(actionName);
        intent.putExtra(STORY, story);
        intent.putExtra(PLAYING_STATE, state);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, PlayingState state) {
        Intent intent = new Intent(actionName);
        intent.putExtra(PLAYING_STATE, state);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, RatingForm ratingForm) {
        Intent intent = new Intent(actionName);
        intent.putExtra(PayloadNames.RATING, ratingForm);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, List<Story> stories) {
        Intent intent = new Intent(actionName);
        intent.putExtra(STORY_PAGE, new ArrayList<>(stories));

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName) {
        Map<String, String> payload = new HashMap<>();
        send(context, actionName, payload);
    }

    private static void send(Context context, String actionName, Map<String, String> payload) {
        Intent intent = new Intent(actionName);

        for (Map.Entry<String, String> payloadEntry : payload.entrySet()) {
            intent.putExtra(payloadEntry.getKey(), payloadEntry.getValue());
        }

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, Boolean value) {
        Intent intent = new Intent(actionName);
        intent.putExtra(STORY_IS_EDGE_PAGE, value);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, Notification notification) {
        Intent intent = new Intent(actionName);
        intent.putExtra(NOTIFICATION, notification);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, File file) {
        Intent intent = new Intent(actionName);
        intent.putExtra(STREAM, file);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, User user) {
        Intent intent = new Intent(actionName);
        intent.putExtra(USER, user);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, UserForm user) {
        Intent intent = new Intent(actionName);
        intent.putExtra(USER_FORM, user);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, TimerSettings timerSettings) {
        Intent intent = new Intent(actionName);
        intent.putExtra(TIMER_SETTING, timerSettings);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void send(Context context, String actionName, Metric metric) {
        Intent intent = new Intent(actionName);
        intent.putExtra(METRICS, metric);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getPayloadObject(Intent intent, String payloadName, Class<T> type) {

        if (null == intent || null == intent.getExtras()) {
            return null;
        }

        return (T) intent.getExtras().get(payloadName);
    }

    @SuppressWarnings("unchecked")
    public static <T> Collection<T> getPayloadCollection(Intent intent, String payloadName, Class<T> type) {
        return (ArrayList) intent.getSerializableExtra(payloadName);
    }

}
