package org.grakovne.mds.android.network.repository;

import org.grakovne.mds.android.network.common.ApiNames;
import org.grakovne.mds.android.network.domain.ApiResponse;
import org.grakovne.mds.android.network.domain.forms.UserForm;
import org.grakovne.mds.android.network.domain.responses.Metric;
import org.grakovne.mds.android.network.domain.responses.User;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserRepository {
    @POST("user")
    Call<ApiResponse<User>> registerUser(@Body UserForm form);

    @DELETE("user/me")
    Call<ApiResponse> forgetUser();

    @GET("user/me")
    Call<ApiResponse<User>> getUserData();

    @GET("user/me/metrics")
    Call<ApiResponse<Metric>> getUserMetrics();

    class ApiFactory {
        public static UserRepository create(OkHttpClient httpClient) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(ApiNames.getApiUrl())
                    .client(httpClient)
                    .build();

            return retrofit.create(UserRepository.class);
        }
    }
}
