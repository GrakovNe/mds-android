package org.grakovne.mds.android.common.enums;

public enum BookmarksConsidering {
    ALWAYS,
    NEVER,
    DEFAULT
}
