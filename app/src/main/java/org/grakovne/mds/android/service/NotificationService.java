package org.grakovne.mds.android.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.network.domain.responses.Story;

import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;


public class NotificationService extends MdsService {

    private Receiver receiver;
    private NotificationManager notificationManager;
    private RemoteViews remoteViews;
    private Notification notification;

    private boolean isNotificationRequested;

    @Override
    public final void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                Player.STARTED_EVENT,
                Player.PLAYED_EVENT,
                Player.PAUSED_EVENT,
                Player.FINISHED_EVENT,
                Player.STARTED_EVENT,
                ActionNames.Notification.PRESSED_EVENT,
                ActionNames.Notification.GOT_INTENT) {
            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Player.STARTED_EVENT:
                        updateNotification(getPayloadObject(intent, STORY, Story.class), settingsManager.isStoryPlaying());
                        break;
                    case Player.PLAYED_EVENT:
                        updateNotification(getPayloadObject(intent, STORY, Story.class), true);
                        break;
                    case Player.PAUSED_EVENT:
                        updateNotification(getPayloadObject(intent, STORY, Story.class), false);
                        break;
                    case Player.FINISHED_EVENT:
                        hideNotification();
                        break;
                    case ActionNames.Notification.GOT_INTENT:
                        processNotificationSending();
                        break;
                    case ActionNames.Notification.PRESSED_EVENT:
                        sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
                        break;
                }
            }
        };
    }

    private void updateNotification(Story story, boolean isPlayed) {
        if (null == story) {
            story = cacheManager.getStory();
        }

        if (null != story) {
            notificationManager.notify(
                    MDS_NOTIFICATION_IDENTITY,
                    buildNotification(story.getTitle(),
                            story.getAuthors(),
                            isPlayed)
            );
        }
    }

    private void processNotificationSending() {
        if (null != notification) {
            send(this, ActionNames.Notification.GOT_EVENT, notification);
            isNotificationRequested = false;
            return;
        }
        isNotificationRequested = true;
    }

    @Override
    public final void unsubscribe() {
        hideNotification();
        receiver.unsubscribe();
        super.unsubscribe();
    }

    @Override
    public final void init() {
        super.init();
        notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        remoteViews = new RemoteViews(this.getPackageName(), R.layout.notification_layout);
        createNotificationChannel();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (!settingsManager.isStoryPlaying()) {
            hideNotification();
        }
        super.onTaskRemoved(rootIntent);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.notification_player),
                    getString(R.string.notification_player),
                    NotificationManager.IMPORTANCE_LOW);

            notificationManager.createNotificationChannel(channel);
        }
    }

    private void hideNotification() {
        notificationManager.cancel(MDS_NOTIFICATION_IDENTITY);
    }

    private Notification buildNotification(String title, String author, Boolean isPlayed) {

        if (null == isPlayed) {
            isPlayed = settingsManager.isStoryPlaying();
        }

        int icon = isPlayed
                ? R.mipmap.ic_notification_pause_button_icon
                : R.mipmap.ic_notification_play_button_icon;

        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_notification_icon)
                .setStyle(new Notification.MediaStyle())
                .setOngoing(isPlayed)
                .setContent(remoteViews);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder.setChannelId(getString(R.string.notification_player));
        }

        remoteViews.setTextViewText(R.id.notification_playing_now_title, title);
        remoteViews.setTextViewText(R.id.notification_playing_now_author, author);
        remoteViews.setImageViewResource(R.id.notification_play_pause_action_button, icon);

        notification = builder.build();

        if (isNotificationRequested) {
            processNotificationSending();
        }

        remoteViews.setOnClickPendingIntent(
                R.id.notification_play_pause_action_button,
                PendingIntent.getBroadcast(this, 0,
                        new Intent(isPlayed ? Player.PAUSED_INTENT : Player.PLAYED_INTENT), 0)
        );

        remoteViews.setOnClickPendingIntent(R.id.notification_layout,
                PendingIntent.getBroadcast(this, 0,
                        new Intent(ActionNames.Notification.PRESSED_EVENT), 0)
        );

        return notification;
    }
}
