package org.grakovne.mds.android.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;

import static org.grakovne.mds.android.common.PayloadNames.STORY_IS_EDGE_PAGE;

public class StoryDashScrollListener extends RecyclerView.OnScrollListener {

    private final Context context;
    private final LinearLayoutManager layoutManager;
    private final CacheManager cacheManager;

    private Boolean isLastPage = false;
    private Boolean isPageLoading = false;

    public StoryDashScrollListener(Context context, LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        this.cacheManager = CacheManager.getManager(context);
        this.context = context;

        initReceiver();
    }

    private void initReceiver() {
        new Receiver(context,
                Network.STORY_IS_LAST_PAGE_DOWNLOADED_EVENT,
                Network.STORY_IS_FIRST_PAGE_DOWNLOADED_EVENT,
                Network.STORY_PAGE_META_DOWNLOADED_INTENT,
                Network.STORY_PAGE_META_DOWNLOADED_EVENT,
                Network.API_ERROR_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Network.STORY_IS_LAST_PAGE_DOWNLOADED_EVENT:
                        Boolean isLastPage = BroadcastManager.getPayloadObject(intent, STORY_IS_EDGE_PAGE, Boolean.class);
                        setIsLastPage(isLastPage);
                        break;

                    case Network.STORY_IS_FIRST_PAGE_DOWNLOADED_EVENT:
                        Boolean isFirstPage = BroadcastManager.getPayloadObject(intent, STORY_IS_EDGE_PAGE, Boolean.class);
                        scrollTop(isFirstPage);
                        break;

                    case Network.STORY_PAGE_META_DOWNLOADED_INTENT:
                        setIsPageLoading(true);
                        break;

                    case Network.STORY_PAGE_META_DOWNLOADED_EVENT:
                    case Network.API_ERROR_EVENT:
                        setIsPageLoading(false);
                        break;
                }
            }
        };
    }

    private void scrollTop(Boolean isFirstPage) {
        if (isFirstPage) {
            layoutManager.scrollToPositionWithOffset(0, 0);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

        super.onScrolled(recyclerView, dx, dy);

        int visibleCount = layoutManager.getChildCount();
        int totalCount = layoutManager.getItemCount();
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();

        if (!isPageLoading
                && !isLastPage
                && (firstVisiblePosition + visibleCount) > (totalCount - visibleCount * 2)
                && firstVisiblePosition > 0) {

            loadMoreItems();
        }
    }

    private void loadMoreItems() {
        incrementCurrentPage();
        BroadcastManager.send(context, Network.STORY_PAGE_META_DOWNLOADED_INTENT);
    }

    private void incrementCurrentPage() {
        Integer currentPage = cacheManager.getSearchPageNumber();
        currentPage++;
        cacheManager.setSearchPageNumber(currentPage);
    }

    private void setIsLastPage(Boolean isLastPage) {
        this.isLastPage = isLastPage;
    }

    private void setIsPageLoading(Boolean isPageLoading) {
        this.isPageLoading = isPageLoading;
    }
}
