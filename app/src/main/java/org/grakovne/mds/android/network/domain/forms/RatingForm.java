package org.grakovne.mds.android.network.domain.forms;

import java.io.Serializable;

public class RatingForm implements Serializable {
    private Integer storyId;
    private Double value;

    public RatingForm(Integer storyId, Double value) {
        this.storyId = storyId;
        this.value = value;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
