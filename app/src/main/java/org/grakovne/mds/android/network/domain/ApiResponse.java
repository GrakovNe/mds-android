package org.grakovne.mds.android.network.domain;

public class ApiResponse<T> {
    private String message;
    private T body;

    public String getMessage() {
        return message;
    }

    public T getBody() {
        return body;
    }
}
