package org.grakovne.mds.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.common.StoryTab;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.service.ApplicationService;

import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.grakovne.mds.android.common.PayloadNames.STORY_PAGE;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadCollection;
import static org.grakovne.mds.android.manager.BroadcastManager.send;

public class DashActivity extends MdsActivity {

    private final Handler handler = new Handler();
    @BindView(R.id.dash_view)
    DashView dashView;
    @BindView(R.id.dash_refresh_container)
    SwipeRefreshLayout dashRefreshContainer;
    @BindView(R.id.play_now_title)
    TextView playNowTitle;
    @BindView(R.id.play_now_author)
    TextView playNowAuthor;
    @BindView(R.id.play_now_play_button)
    ImageButton playNowPlayButton;
    @BindView(R.id.dash_action_bar)
    RelativeLayout dashActionBar;

    private StoryDashAdapter storyDashAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);
        ButterKnife.bind(this);

        initViews();
        initReceivers();

        startService(new Intent(this, ApplicationService.class));
    }

    @Override
    protected void onPostResume() {
        updateActionBar();
        storyDashAdapter.refresh();
        super.onPostResume();
    }

    private void updateActionBar() {
        if (null == cacheManager.getStory()) {
            dashActionBar.setVisibility(GONE);
            return;
        }

        Story story = cacheManager.getStory();

        playNowAuthor.setText(story.getAuthors());
        playNowTitle.setText(story.getTitle());

        updateActionButton(settingsManager.isStoryPlaying());

        dashActionBar.setVisibility(VISIBLE);
    }

    private void updateActionButton(Boolean isPlaying) {
        Integer iconResource = isPlaying ? R.mipmap.dash_pause_icon : R.mipmap.dash_play_icon;
        playNowPlayButton.setImageResource(iconResource);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dash_action_bar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView storySearchView = (SearchView) searchItem.getActionView();
        storySearchView.setQueryHint(getResources().getString(R.string.story_search_placeholder));
        storySearchView.setIconifiedByDefault(false);
        storySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                submitSearchForm(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(() -> submitSearchForm(newText), 500);
                return false;
            }
        });

        String searchStoryType = cacheManager.getSearchType();
        searchItem.setVisible(!searchStoryType.equals(StoryTab.RECENT_STORIES));
        toolbar.setTitle(localizationManager.getStoryType(cacheManager.findStoryType()));

        return true;
    }

    private void initViews() {
        setSupportActionBar(findViewById(R.id.action_toolbar));

        storyDashAdapter = new StoryDashAdapter(dashView);
        dashView.setAdapter(storyDashAdapter);

        dashRefreshContainer.setOnRefreshListener(() -> {
            cacheManager.setSearchPageNumber(null);
            storyDashAdapter.clearContent();
            send(getBaseContext(), Network.STORY_PAGE_META_DOWNLOADED_INTENT);
        });

        dashActionBar.setOnClickListener(view -> send(getApplicationContext(), Ui.SAME_STORY_SELECTED_INTENT));
        dashActionBar.setOnTouchListener((view, event) -> {
            int action = event.getActionMasked();

            switch (action) {
                case (MotionEvent.ACTION_UP):
                    dashActionBar.performClick();
                    return true;
            }

            return DashActivity.super.onTouchEvent(event);
        });

        playNowPlayButton.setOnClickListener(view -> {
            if (settingsManager.isStoryPlaying()) {
                send(getApplicationContext(), Player.PAUSED_INTENT);
            } else {
                send(getApplicationContext(), Player.PLAYED_INTENT);
            }
        });

    }

    private void initReceivers() {
        new Receiver(this,
                Network.STORY_PAGE_META_DOWNLOADED_EVENT,
                Network.API_ERROR_EVENT,
                Ui.NEW_STORIES_TYPE_SELECTED_INTENT,
                Player.PLAYED_EVENT,
                Player.PAUSED_EVENT,
                Player.STOPPED_EVENT,
                Application.STARTED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Network.STORY_PAGE_META_DOWNLOADED_EVENT:
                        Collection<Story> page = getPayloadCollection(intent, STORY_PAGE, Story.class);
                        showStoryPage(page);
                        setDashRefreshing(false);
                        break;

                    case Ui.NEW_STORIES_TYPE_SELECTED_INTENT:
                        changeStoriesType();
                        invalidateOptionsMenu();
                        setDashRefreshing(true);
                        break;

                    case Network.API_ERROR_EVENT:
                        showApiErrorMessage();
                        setDashRefreshing(false);
                        break;

                    case Player.PAUSED_EVENT:
                    case Player.STOPPED_EVENT:
                        updateActionButton(false);
                        break;

                    case Player.PLAYED_EVENT:
                        updateActionButton(true);
                        break;

                    case Application.STARTED_EVENT:
                        send(getBaseContext(), Network.STORY_PAGE_META_DOWNLOADED_INTENT);
                        setDashRefreshing(true);
                        break;
                }
            }
        };
    }

    private void changeStoriesType() {
        storyDashAdapter.clearContent();
        send(this, Ui.NEW_STORIES_TYPE_SELECTED_EVENT);
    }


    private void showStoryPage(Collection<Story> page) {
        storyDashAdapter.addContent(page);
        dashRefreshContainer.setRefreshing(false);
    }

    public void setDashRefreshing(Boolean isRefreshing) {
        dashRefreshContainer.setRefreshing(isRefreshing);
    }

    private void submitSearchForm(String request) {
        String loweredRequest = request.toLowerCase();
        cacheManager.setSearchPageNumber(null);
        cacheManager.setSearchQuery(loweredRequest);

        storyDashAdapter.clearContent();

        send(this, Network.STORY_PAGE_META_DOWNLOADED_INTENT);
    }
}
