package org.grakovne.mds.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.Toolbar;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondarySwitchDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.common.StoryTab;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.manager.SettingsManager;
import org.grakovne.mds.android.network.common.ApiNames;

import static org.grakovne.mds.android.common.SearchParams.BOTH;
import static org.grakovne.mds.android.common.SearchParams.ORDER_ASC;
import static org.grakovne.mds.android.common.SearchParams.ORDER_DESC;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_INTERACTIVE;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_RATING;
import static org.grakovne.mds.android.common.SearchParams.SEARCH_TITLE;
import static org.grakovne.mds.android.common.SearchParams.UNLISTENED;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.ifDebugEnabled;
import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;
import static org.grakovne.mds.android.utils.CommonUtils.isVkAppInstalled;

public class NavigationBar {
    private final Toolbar toolbar;
    private final Activity activity;

    private final CacheManager cacheManager;
    private final SettingsManager settingsManager;

    private Drawer navigation;

    private SecondaryDrawerItem allStoriesItem;
    private SecondaryDrawerItem recentStoriesItem;
    private SecondaryDrawerItem bestStoriesItem;

    private SecondaryDrawerItem randomStoryItem;

    private SecondaryDrawerItem metricItem;
    private SecondaryDrawerItem aboutItem;

    private SecondarySwitchDrawerItem hideListenedItem;
    private SecondarySwitchDrawerItem isAutoPlayItem;

    private AccountHeader accountHeader;

    public NavigationBar(Activity activity, Toolbar toolbar) {
        this.activity = activity;
        this.toolbar = toolbar;

        cacheManager = CacheManager.getManager(activity);
        settingsManager = SettingsManager.getManager(activity);

        initItems();
        initDrawer();

        new Receiver(activity,
                Application.USER_LOGIN_EVENT,
                Application.USER_LOGOUT_EVENT,
                Application.USER_LOGIN_CANCELLED_EVENT) {
            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Application.USER_LOGOUT_EVENT:
                        whenUserLogout();
                        break;
                    case Application.USER_LOGIN_EVENT:
                        whenUserLogin();
                        break;
                }
            }
        };
    }

    private void whenUserLogin() {
        accountHeader.updateProfile(collectUserAccount());
        hideListenedItem.withChecked(true);
        navigation.updateItem(hideListenedItem);
        settingsManager.setSearchStoryListenedType(UNLISTENED);
        sendStoryPageTypeChangedBroadcast();
    }

    private void whenUserLogout() {
        accountHeader.updateProfile(collectUserAccount());
        settingsManager.setSearchStoryListenedType(BOTH);
        hideListenedItem.withChecked(false);
        navigation.updateItem(hideListenedItem);

        cacheManager.setSearchType(SEARCH_INTERACTIVE);
        cacheManager.setSearchOrderField(SEARCH_TITLE);
        cacheManager.setSearchDirection(ORDER_ASC);

        sendStoryPageTypeChangedBroadcast();
    }

    private void initDrawer() {
        DrawerBuilder builder = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(accountHeader)
                .withSelectedItem(-1)
                .addDrawerItems(
                        allStoriesItem,
                        randomStoryItem,
                        bestStoriesItem,
                        recentStoriesItem,

                        new SectionDrawerItem().withName(R.string.drawer_item_settings),

                        isAutoPlayItem,
                        hideListenedItem,

                        new DividerDrawerItem(),

                        metricItem,
                        aboutItem
                );

        ifDebugEnabled(() -> builder.withStickyFooter(R.layout.navigation_footer));
        navigation = builder.build();
    }

    private void initItems() {
        allStoriesItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_main).withIcon(FontAwesome.Icon.faw_home)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    cacheManager.setSearchType(SEARCH_INTERACTIVE);
                    cacheManager.setSearchOrderField(SEARCH_TITLE);
                    cacheManager.setSearchDirection(ORDER_ASC);
                    sendStoryPageTypeChangedBroadcast();
                    hideBar();

                    return true;
                });

        randomStoryItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_random_story)
                .withIcon(FontAwesome.Icon.faw_random)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    send(activity, Network.RANDOM_STORY_META_DOWNLOADED_INTENT);
                    hideBar();

                    return true;
                });

        bestStoriesItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_best_stories)
                .withIcon(FontAwesome.Icon.faw_star)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    cacheManager.setSearchType(SEARCH_INTERACTIVE);
                    cacheManager.setSearchOrderField(SEARCH_RATING);
                    cacheManager.setSearchDirection(ORDER_DESC);
                    sendStoryPageTypeChangedBroadcast();
                    hideBar();

                    return true;
                });

        recentStoriesItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_recent_stories)
                .withIcon(FontAwesome.Icon.faw_history)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    ifUserLogged(activity, () -> {
                        cacheManager.setSearchType(StoryTab.RECENT_STORIES);
                        sendStoryPageTypeChangedBroadcast();
                        hideBar();
                    });
                    return true;
                });

        hideListenedItem = new SecondarySwitchDrawerItem()
                .withName(R.string.drawer_item_hide_listened)
                .withIcon(FontAwesome.Icon.faw_headphones)
                .withChecked(settingsManager.getSearchStoryListenedType().equals(UNLISTENED))
                .withSelectable(false)
                .withOnCheckedChangeListener((drawerItem, buttonView, isChecked) -> {
                    ifUserLogged(activity, () -> {
                        String listenedType = isChecked ? UNLISTENED : BOTH;
                        settingsManager.setSearchStoryListenedType(listenedType);
                        sendStoryPageTypeChangedBroadcast();
                    });
                });

        isAutoPlayItem = new SecondarySwitchDrawerItem()
                .withName(R.string.drawer_auto_play)
                .withIcon(FontAwesome.Icon.faw_play_circle)
                .withChecked(settingsManager.isAutoPlay())
                .withSelectable(false)
                .withOnCheckedChangeListener((drawerItem, buttonView, isChecked) -> {
                    settingsManager.setAutoPlay(isChecked);
                });

        metricItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_metrics)
                .withIcon(FontAwesome.Icon.faw_info_circle)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    ifUserLogged(activity, () -> {
                        send(activity, Ui.METRICS_INTENT);
                        hideBar();
                    });

                    return true;
                });

        aboutItem = new SecondaryDrawerItem()
                .withName(R.string.drawer_item_about)
                .withIcon(FontAwesome.Icon.faw_user)
                .withSelectable(false)
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    showAbout();
                    hideBar();
                    return true;
                });

        accountHeader = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.drawable_header_image)
                .addProfiles(collectUserAccount())
                .withSelectionListEnabled(false)
                .withOnAccountHeaderListener((view, profile, current) -> {
                    ifUserLogged(activity, () -> {
                        new ConfirmDialog(activity,
                                activity.getString(R.string.log_out_dialog_title),
                                activity.getString(R.string.log_out_dialog_text),
                                () -> send(activity, Application.USER_LOGOUT_INTENT));
                    }, () -> send(activity, Application.USER_LOGIN_INTENT));
                    return true;
                })
                .build();
    }

    private void showAbout() {
        Intent intent = isVkAppInstalled(activity) ?
                new Intent(Intent.ACTION_VIEW, Uri.parse(ApiNames.getVkPublicUrl())) :
                new Intent(Intent.ACTION_VIEW, Uri.parse(ApiNames.getGrakovneBlogUrl()));

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private void sendStoryPageTypeChangedBroadcast() {
        cacheManager.setSearchPageNumber(null);

        send(activity.getBaseContext(), Ui.NEW_STORIES_TYPE_SELECTED_INTENT);
        send(activity.getBaseContext(), Network.STORY_PAGE_META_DOWNLOADED_INTENT);
    }

    private void hideBar() {
        if (null != navigation) {
            navigation.closeDrawer();
        }
    }

    private IProfile collectUserAccount() {
        IProfile currentProfile;

        if (null != accountHeader) {
            currentProfile = accountHeader.getActiveProfile();
        } else {
            currentProfile = new ProfileDrawerItem();
        }

        if (cacheManager.isUserLogged()) {
            currentProfile.withName(cacheManager.getUserName());
            currentProfile.withEmail(cacheManager.getUserEmail());
            currentProfile.withIcon(cacheManager.getUserImageUrl());

        } else {
            currentProfile.withName(null);
            currentProfile.withEmail(activity.getString(R.string.navigation_bar_invitation_to_login));
            currentProfile.withIcon(R.drawable.user_image);
        }

        return currentProfile;
    }
}
