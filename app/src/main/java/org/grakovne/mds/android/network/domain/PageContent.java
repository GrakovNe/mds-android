package org.grakovne.mds.android.network.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PageContent<T> implements Serializable {
    private T content;

    @SerializedName("last")
    private Boolean isLastPage;

    @SerializedName("first")
    private Boolean isFirstPage;

    private Integer totalPages;

    public T getContent() {
        return content;
    }

    public Boolean getLastPage() {
        return isLastPage;
    }

    public Boolean getFirstPage() {
        return isFirstPage;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

}
