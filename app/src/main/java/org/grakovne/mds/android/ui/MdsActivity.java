package org.grakovne.mds.android.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.manager.LocalizationManager;
import org.grakovne.mds.android.manager.SettingsManager;
import org.grakovne.mds.android.network.domain.forms.UserForm;

import java.io.Serializable;

import static com.google.android.gms.auth.api.Auth.GoogleSignInApi;
import static com.google.android.gms.auth.api.signin.GoogleSignInOptions.DEFAULT_SIGN_IN;
import static org.grakovne.mds.android.utils.AccountUtils.getUserAccountPassword;

@SuppressLint("Registered")
public class MdsActivity extends AppCompatActivity implements Serializable {
    private static final int GOOGLE_SIGN_IN_REQUEST_CODE = 8962;
    protected SettingsManager settingsManager;
    protected CacheManager cacheManager;
    protected LocalizationManager localizationManager;
    protected NavigationBar navigationBar;
    protected Toolbar toolbar;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingsManager = SettingsManager.getManager(this);
        cacheManager = CacheManager.getManager(this);
        localizationManager = LocalizationManager.getManager(this);

        initAuth();
        connectAuth();
        initReceivers();
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        this.toolbar = toolbar;
        setNavigationBar();
    }

    private void setNavigationBar() {
        if (null == navigationBar) {
            navigationBar = new NavigationBar(this, toolbar);
        }
    }

    protected void showApiErrorMessage() {
        Toast.makeText(this, R.string.server_error_message, Toast.LENGTH_SHORT).show();
    }

    private void initAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestId()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void connectAuth() {
        if (null != googleApiClient) {
            googleApiClient.connect();
        }
    }

    private void initReceivers() {
        new Receiver(this,
                Application.USER_LOGIN_INTENT,
                Application.USER_LOGOUT_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Application.USER_LOGIN_INTENT:
                        startGoogleLogin();
                        break;

                    case Application.USER_LOGOUT_INTENT:
                        startLogout();
                        break;
                }
            }
        };
    }

    private void startLogout() {
        GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(status -> {
            if (!status.isSuccess()) {
                BroadcastManager.send(getApplicationContext(), Network.API_ERROR_EVENT);
            }
        });
    }

    private void startGoogleLogin() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int status = apiAvailability.isGooglePlayServicesAvailable(this);

        if (status != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(status)) {
                apiAvailability
                        .getErrorDialog(this, status, GOOGLE_SIGN_IN_REQUEST_CODE)
                        .show();
            } else {
                Toast.makeText(this, R.string.auth_services_error_message, Toast.LENGTH_LONG).show();
            }

            return;
        }

        startActivityForResult(GoogleSignInApi.getSignInIntent(googleApiClient), GOOGLE_SIGN_IN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GOOGLE_SIGN_IN_REQUEST_CODE) {
            GoogleSignInResult result = GoogleSignInApi.getSignInResultFromIntent(data);

            if (null != result && result.isSuccess() && null != result.getSignInAccount()) {
                final GoogleSignInAccount account = result.getSignInAccount();
                persistAccountInfo(account);

                UserForm form = new UserForm(account.getId(), getUserAccountPassword(account));
                BroadcastManager.send(getApplicationContext(), Application.USER_REGISTERED_INTENT, form);
            } else {
                BroadcastManager.send(getApplicationContext(), Application.USER_LOGIN_CANCELLED_EVENT);
            }
        }
    }

    private void persistAccountInfo(GoogleSignInAccount account) {
        cacheManager.setUserEmail(account.getEmail());
        cacheManager.setUserName(account.getDisplayName());
        cacheManager.setUserImageUrl(account.getPhotoUrl());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
