package org.grakovne.mds.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageButton;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.network.domain.responses.Story;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.grakovne.mds.android.common.PayloadNames.STORY;

public class CommentsActivity extends MdsActivity {

    @BindView(R.id.comments_toolbar)
    Toolbar commentsToolbar;
    @BindView(R.id.comments_view)
    RecyclerView commentsView;
    @BindView(R.id.comments_refresh_container)
    SwipeRefreshLayout commentsRefreshContainer;
    @BindView(R.id.comment_text_field)
    EditText commentTextField;
    @BindView(R.id.submit_comment_button)
    ImageButton submitButton;
    private Story story;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);

        initReceivers();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        story = BroadcastManager.getPayloadObject(getIntent(), STORY, Story.class);
        if (null == story) {
            story = cacheManager.getStory();
        }
    }

    private void initReceivers() {

        new Receiver(this) {
            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    // todo: implement here
                }
            }
        };
    }
}
