package org.grakovne.mds.android.ui;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import org.grakovne.mds.android.R;
import org.grakovne.mds.android.common.ActionNames;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.media.PlayingState;
import org.grakovne.mds.android.media.TimerSettings;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.utils.CacheUtils;
import org.grakovne.mds.android.utils.ContentUtils;
import org.grakovne.mds.android.utils.NetworkUtils;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.deweyreed.scrollhmspicker.ScrollHmsPickerBuilder;
import ru.alexbykov.nopermission.PermissionHelper;

import static android.widget.Toast.LENGTH_SHORT;
import static org.grakovne.mds.android.R.string.story_caching_toast;
import static org.grakovne.mds.android.common.ActionNames.Cache;
import static org.grakovne.mds.android.common.PayloadNames.PLAYING_STATE;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;
import static org.grakovne.mds.android.utils.CommonUtils.ifSameStory;

public class StoryActivity extends MdsActivity {

    @BindView(R.id.story_title)
    TextView storyTitle;
    @BindView(R.id.story_cover)
    ImageView storyCover;
    @BindView(R.id.story_author)
    TextView storyAuthor;
    @BindView(R.id.rating_container)
    RatingView ratingView;
    @BindView(R.id.story_container)
    RelativeLayout storyView;
    @BindView(R.id.story_annotation)
    JustifiedTextView storyAnnotation;
    @BindView(R.id.story_current_time)
    TextView storyCurrentTime;
    @BindView(R.id.story_length)
    TextView storyLength;
    @BindView(R.id.action_button)
    ImageButton actionButton;
    @BindView(R.id.timer_button)
    ImageView timerButton;
    @BindView(R.id.offline_button)
    ImageView offlineButton;
    @BindView(R.id.seek_bar)
    SeekBar seekBar;
    @BindView(R.id.story_action_bar)
    RelativeLayout actionBar;

    private Story story;
    private Timer stateInfoTimer;
    private PermissionHelper externalStoragePermissionChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        externalStoragePermissionChecker = new PermissionHelper(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initReceivers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        toolbar.setTitle(R.string.full_application_name);
        getMenuInflater().inflate(R.menu.story_action_bar_menu, menu);

        MenuItem commentItem = menu.findItem(R.id.story_action_comment);
        commentItem.setOnMenuItemClickListener(item -> {
            send(this, Ui.STORY_COMMENTS_INTENT, story);
            Toast.makeText(StoryActivity.this, R.string.comments_coming_soon_stub_toast, Toast.LENGTH_LONG).show();
            return false;
        });

        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        initViews();
        initTimers();

        story = cacheManager.getStory();
        if (null == story) {
            story = getPayloadObject(getIntent(), STORY, Story.class);
        }

        drawStoryData(story);
        requestPlayingState();
        updateActionButton(settingsManager.isStoryPlaying());
        updateTimerButton(settingsManager.isTimerEnabled());
        updateCachedButton(CacheUtils.isStreamCached(this, story));
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onPause() {
        stopTimers();
        super.onPause();
    }

    private void stopTimers() {
        if (null != stateInfoTimer) {
            stateInfoTimer.cancel();
        }
    }

    private void drawStoryData(Story story) {
        if (null == story) {
            return;
        }

        storyTitle.setText(story.getTitle());

        if (TextUtils.isEmpty(story.getAnnotation())) {
            storyAnnotation.setText(R.string.no_annotated_story_stub);
        } else {
            storyAnnotation.setText(story.getAnnotation());
        }

        storyAuthor.setText(story.getAuthors());

        Picasso
                .with(this)
                .load(NetworkUtils.findImageUrl(story))
                .placeholder(R.drawable.story_no_cover_image)
                .into(storyCover);

        ratingView.drawRating(story);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        externalStoragePermissionChecker.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void initViews() {
        setSupportActionBar(findViewById(R.id.detailed_action_toolbar));

        actionButton.setOnClickListener(view ->
                send(this, settingsManager.isStoryPlaying()
                        ? Player.PAUSED_INTENT
                        : Player.PLAYED_INTENT)
        );

        timerButton.setOnClickListener(view -> {
            if (settingsManager.isTimerEnabled()) {
                send(this, ActionNames.Timer.DISABLED_INTENT);
                return;
            }

            showTimerDialog();
        });

        offlineButton.setOnClickListener(v -> {
            if (!CacheUtils.isStreamCached(this, story)) {
                externalStoragePermissionChecker.check(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .onSuccess(() -> {
                            send(StoryActivity.this, Cache.CACHED_STREAM_INTENT, story);
                            Toast.makeText(StoryActivity.this, story_caching_toast, LENGTH_SHORT).show();
                        })
                        .onDenied(() -> Toast.makeText(StoryActivity.this, R.string.story_caching_fault_permission, Toast.LENGTH_LONG).show())
                        .onNeverAskAgain(() -> externalStoragePermissionChecker.startApplicationSettingsActivity())
                        .run();
                return;
            }

            showCacheRemoveDialog();
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    updateTimeLine(seekBar.getProgress(), story.getLength());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopTimers();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                PlayingState state = new PlayingState(story,
                        story.getLength(),
                        seekBar.getProgress());

                send(getBaseContext(), Player.SOUGHT_INTENT, state);
                initTimers();
            }
        });
    }

    private void showCacheRemoveDialog() {
        new ConfirmDialog(this,
                getString(R.string.dash_remove_from_cache_title),
                getString(R.string.dash_remove_from_cache_message),
                () -> send(StoryActivity.this, Cache.CACHE_REMOVED_STREAM_INTENT, story));
    }

    private void showTimerDialog() {
        new ScrollHmsPickerBuilder(getSupportFragmentManager(), (reference, hours, minutes, seconds) -> {
            Toast.makeText(StoryActivity.this, R.string.timer_enabled_toast, LENGTH_SHORT).show();
            send(getApplicationContext(),
                    ActionNames.Timer.ENABLED_INTENT,
                    new TimerSettings(hours, minutes, seconds)
            );
        })
                .setAutoStep(true)
                .setColorSelected(R.color.colorAccent)
                .show();
    }

    private void initTimers() {
        stateInfoTimer = new Timer();
        stateInfoTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                requestPlayingState();
            }
        }, 0, 1000);
    }

    private void requestPlayingState() {
        send(getApplicationContext(), Player.SENT_STATE_INFO_INTENT);
    }

    private void initReceivers() {
        new Receiver(this,
                Player.PLAYED_EVENT,
                Player.STOPPED_EVENT,
                Player.PAUSED_EVENT,
                Player.FINISHED_EVENT,
                Player.STARTED_EVENT,
                Player.SENT_STATE_INFO_EVENT,
                ActionNames.Timer.DISABLED_EVENT,
                ActionNames.Timer.ENABLED_EVENT,
                Network.STORY_META_DOWNLOADED_EVENT,
                Network.API_ERROR_EVENT,
                Cache.CACHED_STREAM_EVENT,
                Cache.CACHE_REMOVED_STREAM_EVENT,
                Ui.STORY_BOOKMARKS_CONSIDERED_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                Story received = getPayloadObject(intent, STORY, Story.class);
                story = received == null ? story : received;

                switch (action) {
                    case Player.STARTED_EVENT:
                        updateActionBar(story);
                        updateActionButton(settingsManager.isStoryPlaying());
                        break;

                    case Player.PLAYED_EVENT:
                        updateActionButton(true);
                        break;

                    case Cache.CACHE_REMOVED_STREAM_EVENT:
                        ifSameStory(StoryActivity.this, received, () -> updateCachedButton(false), emptyAction());
                        break;

                    case Cache.CACHED_STREAM_EVENT:
                        ifSameStory(StoryActivity.this, received, () -> updateCachedButton(true), emptyAction());
                        break;

                    case Network.API_ERROR_EVENT:
                        showApiErrorMessage();
                        break;

                    case Player.STOPPED_EVENT:
                    case Player.PAUSED_EVENT:
                        updateActionButton(false);
                        break;

                    case Player.SENT_STATE_INFO_EVENT:
                        PlayingState state = getPayloadObject(intent, PLAYING_STATE, PlayingState.class);
                        updateTimeLine(state.getPosition(), state.getDuration());
                        break;

                    case Network.STORY_META_DOWNLOADED_EVENT:
                        drawStoryData(story);
                        break;

                    case ActionNames.Timer.DISABLED_EVENT:
                        updateTimerButton(false);
                        break;

                    case ActionNames.Timer.ENABLED_EVENT:
                        updateTimerButton(true);
                        break;

                    case Ui.STORY_BOOKMARKS_CONSIDERED_INTENT:
                        new BookmarksDialog(StoryActivity.this);
                        break;
                }
            }
        };

        send(this, Ui.SCREEN_SWITCHED_EVENT, this);
    }

    private void updateActionBar(Story story) {
        storyTitle.setText(story.getTitle());
        storyAuthor.setText(story.getAuthors());
    }

    private void updateTimeLine(int storyPosition, int storyDuration) {
        storyCurrentTime.setText(ContentUtils.parseLength(storyPosition));
        storyLength.setText(ContentUtils.parseLength(storyDuration));

        seekBar.setMax(storyDuration);
        seekBar.setProgress(storyPosition);
    }

    private void updateActionButton(boolean isPlaying) {
        Integer iconResource = isPlaying ? R.mipmap.pause_icon : R.mipmap.play_icon;
        actionButton.setImageResource(iconResource);
    }

    private void updateTimerButton(boolean enabled) {
        Integer iconResource = enabled ? R.mipmap.player_timer_on : R.mipmap.player_timer_off;
        timerButton.setImageResource(iconResource);
    }

    private void updateCachedButton(boolean cached) {
        Integer iconResource = cached ? R.mipmap.action_bar_offline_story_icon : R.mipmap.action_bar_online_story_icon;
        offlineButton.setImageResource(iconResource);
    }
}
