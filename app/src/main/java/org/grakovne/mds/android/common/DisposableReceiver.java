package org.grakovne.mds.android.common;

import android.content.Context;
import android.content.Intent;

public abstract class DisposableReceiver extends Receiver {

    public DisposableReceiver(Context context, String... actions) {
        super(context, actions);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        this.unsubscribe();
        super.onReceive(context, intent);
    }
}
