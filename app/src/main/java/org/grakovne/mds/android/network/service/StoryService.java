package org.grakovne.mds.android.network.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Application;
import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.CacheManager;
import org.grakovne.mds.android.manager.NetworkManager;
import org.grakovne.mds.android.manager.SettingsManager;
import org.grakovne.mds.android.media.PlayingState;
import org.grakovne.mds.android.network.common.FaultTolerantQueryResult;
import org.grakovne.mds.android.network.common.QueryResult;
import org.grakovne.mds.android.network.domain.ApiResponse;
import org.grakovne.mds.android.network.domain.PageContent;
import org.grakovne.mds.android.network.domain.forms.BookmarkForm;
import org.grakovne.mds.android.network.domain.forms.RatingForm;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.network.repository.StoryRepository;
import org.grakovne.mds.android.service.MdsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.OkHttpClient;
import retrofit2.Response;

import static org.grakovne.mds.android.common.PayloadNames.PLAYING_STATE;
import static org.grakovne.mds.android.common.PayloadNames.RATING;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.common.PayloadNames.STORY_PAGE_NUMBER;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;
import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;

public class StoryService extends MdsService {

    private static final Integer DEFAULT_PAGE_NUMBER = -1;

    private StoryRepository storyRepository;
    private SettingsManager settingsManager;
    private CacheManager cacheManager;

    private Receiver receiver;

    @Override
    protected void init() {
        OkHttpClient client = NetworkManager.getManager(getContext()).getApiClient();
        storyRepository = StoryRepository.ApiFactory.create(client);
        settingsManager = SettingsManager.getManager(getContext());
        cacheManager = CacheManager.getManager(getContext());
    }

    @Override
    protected void subscribe() {
        super.subscribe();
        receiver = new Receiver(this, Network.STORY_META_DOWNLOADED_INTENT,
                Network.RANDOM_STORY_META_DOWNLOADED_INTENT,
                Network.STORY_PAGE_META_DOWNLOADED_INTENT,
                Network.STORY_BOOKMARKED_INTENT,
                Network.STORY_LISTENED_INTENT,
                Network.STORY_RATED_INTENT,
                Network.STORY_ADD_RECENT_INTENT,
                Network.STORY_REMOVE_RECENT_INTENT,
                Application.USER_LOGIN_EVENT,
                Application.USER_LOGOUT_EVENT,
                Player.PLAYED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Network.STORY_META_DOWNLOADED_INTENT:
                        fetchStory(getPayloadObject(intent, STORY, Story.class));
                        break;

                    case Network.RANDOM_STORY_META_DOWNLOADED_INTENT:
                        fetchRandomStory();
                        break;

                    case Network.STORY_PAGE_META_DOWNLOADED_INTENT:
                        Integer page = intent.getIntExtra(STORY_PAGE_NUMBER, DEFAULT_PAGE_NUMBER);
                        fetchStories(page);
                        break;

                    case Network.STORY_BOOKMARKED_INTENT:
                        PlayingState state = getPayloadObject(intent, PLAYING_STATE, PlayingState.class);
                        bookmarkStory(state);
                        break;

                    case Network.STORY_LISTENED_INTENT:
                        listenedStory(getPayloadObject(intent, STORY, Story.class));
                        break;

                    case Network.STORY_RATED_INTENT:
                        RatingForm userRatingForm = getPayloadObject(intent, RATING, RatingForm.class);
                        rateStory(userRatingForm);
                        break;

                    case Network.STORY_ADD_RECENT_INTENT:
                    case Player.PLAYED_EVENT:
                        ifUserLogged(getContext(), () -> addRecent(getPayloadObject(intent, STORY, Story.class)), emptyAction());
                        break;

                    case Network.STORY_REMOVE_RECENT_INTENT:
                        ifUserLogged(getContext(), () -> removeRecent(getPayloadObject(intent, STORY, Story.class)), emptyAction());
                        break;

                    case Application.USER_LOGIN_EVENT:
                    case Application.USER_LOGOUT_EVENT:
                        init();
                        break;
                }
            }
        };
    }

    private void addRecent(Story story) {
        story = null == story ? cacheManager.getStory() : story;
        storyRepository.addRecent(story.getId()).enqueue(new FaultTolerantQueryResult<ApiResponse>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse> response) {
                send(getContext(), Network.STORY_ADD_RECENT_EVENT);
            }
        });
    }

    private void removeRecent(Story story) {
        storyRepository.removeRecent(story.getId()).enqueue(new FaultTolerantQueryResult<ApiResponse>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse> response) {
                send(getContext(), Network.STORY_REMOVE_RECENT_EVENT);
            }
        });
    }

    private void rateStory(RatingForm ratingForm) {
        storyRepository.rateStory(ratingForm.getStoryId(), ratingForm).enqueue(new FaultTolerantQueryResult<ApiResponse<Story>>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse<Story>> response) {
                send(getContext(), Network.STORY_DOWNLOAD_RATING_EVENT, response.body().getBody());
            }
        });
    }

    private void bookmarkStory(PlayingState state) {
        BookmarkForm form = new BookmarkForm();
        form.setTimestamp(state.getPosition());

        storyRepository.bookmarkStory(state.getStory().getId(), form).enqueue(new FaultTolerantQueryResult<Void>(getContext()) {
            @Override
            public void onResult(Response<Void> response) {
                send(getContext(), Network.STORY_BOOKMARKED_EVENT);
            }
        });
    }

    private void listenedStory(Story story) {
        storyRepository.makeStoryAsListened(story.getId()).enqueue(new FaultTolerantQueryResult<ApiResponse>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse> response) {
                send(getContext(), Network.STORY_LISTENED_EVENT);
            }
        });
    }

    private void fetchStories(Integer pageNumber) {
        if (Objects.equals(DEFAULT_PAGE_NUMBER, pageNumber)) {
            pageNumber = cacheManager.getSearchPageNumber();
        } else {
            cacheManager.setSearchPageNumber(pageNumber);
        }

        Map<String, String> storySearchMap = getSearchParameters();
        String storySearchUrl = prepareQuery(pageNumber, storySearchMap);

        storyRepository
                .getStoryPage(storySearchUrl)
                .enqueue(new QueryResult<ApiResponse<PageContent<List<Story>>>>(getContext()) {
                    @Override
                    public void onResult(Response<ApiResponse<PageContent<List<Story>>>> response) {
                        send(getContext(),
                                Network.STORY_PAGE_META_DOWNLOADED_EVENT,
                                response.body().getBody().getContent()
                        );

                        send(getContext(),
                                Network.STORY_IS_LAST_PAGE_DOWNLOADED_EVENT,
                                response.body().getBody().getLastPage()
                        );

                        send(getContext(),
                                Network.STORY_IS_FIRST_PAGE_DOWNLOADED_EVENT,
                                response.body().getBody().getFirstPage()
                        );
                    }
                });
    }

    private void fetchRandomStory() {
        storyRepository.getRandomStory().enqueue(new QueryResult<ApiResponse<Story>>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse<Story>> response) {
                send(getContext(),
                        Network.STORY_META_DOWNLOADED_EVENT,
                        response.body().getBody());
            }
        });
    }

    private void fetchStory(final Story story) {
        storyRepository.getStory(story.getId()).enqueue(new QueryResult<ApiResponse<Story>>(getContext()) {
            @Override
            public void onResult(Response<ApiResponse<Story>> response) {
                send(getContext(),
                        Network.STORY_META_DOWNLOADED_EVENT,
                        response.body().getBody());
            }
        });
    }

    @Override
    public void unsubscribe() {
        receiver.unsubscribe();
    }

    private String prepareQuery(int page, Map<String, String> params) {
        StringBuilder resultUrl = new StringBuilder();

        resultUrl.append("story/")
                .append(cacheManager.getSearchType())
                .append("?page=")
                .append(page);

        for (Map.Entry<String, String> entry : params.entrySet()) {
            resultUrl
                    .append("&")
                    .append(entry.getKey())
                    .append("=")
                    .append(entry.getValue());
        }

        return resultUrl.toString();
    }

    private Map<String, String> getSearchParameters() {
        Map<String, String> result = new HashMap<>(5);
        result.put("orderBy", cacheManager.getSearchOrderField());
        result.put("orderDirection", cacheManager.getSearchDirection());
        result.put("listenedType", settingsManager.getSearchStoryListenedType());
        result.put("title", cacheManager.getSearchQuery());
        result.put("author", cacheManager.getSearchQuery());
        return result;
    }
}
