package org.grakovne.mds.android.utils;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.grakovne.mds.android.network.domain.forms.UserForm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.Credentials;

public class AccountUtils {

    public static String getUserAccountPassword(GoogleSignInAccount account) {
        return hashString(account.getId() + account.getEmail()) + getApplicationSalt();
    }

    public static String getAuthToken(UserForm user) {
        return Credentials.basic(user.getUsername(), user.getPassword());
    }

    private static String getApplicationSalt() {
        String saltSource = "7e7b19d4"
                .concat("a3b3")
                .concat("11e7")
                .concat("abc4")
                .concat("cec278b6b50a");

        return hashString(saltSource);
    }

    private static String hashString(String originString) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(originString.getBytes());

            StringBuilder stringBuilder = new StringBuilder();
            for (byte b : bytes) {
                stringBuilder.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }

            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

}
