package org.grakovne.mds.android.service;

import android.content.Intent;

import org.grakovne.mds.android.common.ActionNames.Network;
import org.grakovne.mds.android.common.ActionNames.Player;
import org.grakovne.mds.android.common.ActionNames.Ui;
import org.grakovne.mds.android.common.DisposableReceiver;
import org.grakovne.mds.android.common.Receiver;
import org.grakovne.mds.android.manager.BroadcastManager;
import org.grakovne.mds.android.media.CachedPlayer;
import org.grakovne.mds.android.media.DataSource;
import org.grakovne.mds.android.media.NetworkPlayer;
import org.grakovne.mds.android.media.PlayingState;
import org.grakovne.mds.android.media.StoryPlayer;
import org.grakovne.mds.android.network.domain.responses.Story;
import org.grakovne.mds.android.ui.MdsActivity;
import org.grakovne.mds.android.ui.StoryActivity;

import static org.grakovne.mds.android.common.ActionNames.Network.STORY_LISTENED_INTENT;
import static org.grakovne.mds.android.common.ActionNames.Player.FINISHED_INTENT;
import static org.grakovne.mds.android.common.ActionNames.Player.SENT_STATE_INFO_INTENT;
import static org.grakovne.mds.android.common.PayloadNames.PLAYING_STATE;
import static org.grakovne.mds.android.common.PayloadNames.SCREEN;
import static org.grakovne.mds.android.common.PayloadNames.STORY;
import static org.grakovne.mds.android.manager.BroadcastManager.getPayloadObject;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CacheUtils.isStreamCached;
import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;
import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;

public class PlayerService extends MdsService {
    private Receiver receiver;
    private StoryPlayer storyPlayer;

    @Override
    protected void subscribe() {
        super.subscribe();

        receiver = new Receiver(this,
                Player.STARTED_INTENT,
                Player.PLAYED_INTENT,
                Player.PAUSED_INTENT,
                Player.STOPPED_INTENT,
                Player.SOUGHT_INTENT,
                Player.PREPARED_EVENT,
                Player.FINISHED_INTENT,
                Player.SENT_STATE_INFO_INTENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Player.STARTED_INTENT:
                        Story story = getPayloadObject(intent, STORY, Story.class);
                        PlayingState state = getPayloadObject(intent, PLAYING_STATE, PlayingState.class);

                        if (null == state) {
                            startStory(story);
                        } else {
                            startStory(story, state.getPosition());
                        }

                        break;
                    case Player.PLAYED_INTENT:
                        playStory();
                        break;
                    case Player.PAUSED_INTENT:
                        pauseStory();
                        break;
                    case Player.STOPPED_INTENT:
                        stopStory();
                        break;
                    case Player.SOUGHT_INTENT:
                        seekStory(getPayloadObject(intent, PLAYING_STATE, PlayingState.class));
                        break;
                    case Player.PREPARED_EVENT:
                        if (settingsManager.isStoryPlaying()) {
                            playStory();
                            sendStateInfo();
                        }
                        break;
                    case FINISHED_INTENT:
                        finishPlaying();
                        break;
                    case SENT_STATE_INFO_INTENT:
                        sendStateInfo();
                        break;
                }
            }
        };
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (null != storyPlayer && settingsManager.isStoryPlaying()) {
            BroadcastManager.send(this,
                    Network.STORY_BOOKMARKED_INTENT,
                    storyPlayer.getCurrentPlayingState());
        }
        super.onTaskRemoved(rootIntent);
    }

    private void seekStory(PlayingState state) {

        startStory(state.getStory(), state.getPosition());
        send(this, Player.SOUGHT_EVENT);
    }

    private void finishPlaying() {
        ifUserLogged(this, () -> send(PlayerService.this,
                STORY_LISTENED_INTENT,
                storyPlayer.getDataSource().getStory()));

        sendStateInfo(new PlayingState(
                storyPlayer.getDataSource().getStory(),
                storyPlayer.getDataSource().getStory().getLength(),
                0
        ));
        startStory(cacheManager.getStory(), 0);
        pauseStory();
    }

    private void stopStory() {
        if (null == storyPlayer) {
            return;
        }

        storyPlayer.stop();
        settingsManager.setStoryPlaying(false);
        send(this, Player.STOPPED_EVENT);
    }

    private void pauseStory() {
        if (null == storyPlayer) {
            return;
        }

        storyPlayer.pause();
        settingsManager.setStoryPlaying(false);
        send(this, Player.PAUSED_EVENT);

        ifUserLogged(this, () -> send(
                PlayerService.this,
                Network.STORY_BOOKMARKED_INTENT,
                storyPlayer.getCurrentPlayingState()),
                emptyAction()
        );

    }

    private void playStory() {
        if (null == storyPlayer) {
            return;
        }

        storyPlayer.play();
        settingsManager.setStoryPlaying(true);
        send(this, Player.PLAYED_EVENT);
    }

    private void sendStateInfo() {
        if (null == storyPlayer) {
            return;
        }

        PlayingState state = storyPlayer.getCurrentPlayingState();

        if (null == state) {
            return;
        }

        sendStateInfo(state);
    }

    private void sendStateInfo(PlayingState state) {
        send(this, Player.SENT_STATE_INFO_EVENT, state);
    }

    private void startStory(Story story) {
        if (null == story.getProgress() || story.getProgress() == 0) {
            startStory(story, 0);
            return;
        }

        switch (settingsManager.getBookmarksConsideringType()) {
            case NEVER:
                startStory(story, 0);
                break;
            case ALWAYS:
                startStory(story, story.getProgress());
                break;
            default:
                askBookmarksConsidered(story);
        }

    }

    private void askBookmarksConsidered(Story story) {
        new DisposableReceiver(this,
                Ui.STORY_FROM_LAST_BOOKMARK_PLAYED_EVENT,
                Ui.STORY_FROM_START_PLAYED_EVENT) {

            @Override
            public void onAction(Intent intent, String action) {
                switch (action) {
                    case Ui.STORY_FROM_LAST_BOOKMARK_PLAYED_EVENT:
                        startStory(story, story.getProgress());
                        break;

                    case Ui.STORY_FROM_START_PLAYED_EVENT:
                        startStory(story, 0);
                        break;
                }
            }
        };

        send(getContext(), Ui.STORY_BOOKMARKS_CONSIDERED_INTENT);

        new DisposableReceiver(this, Ui.SCREEN_SWITCHED_EVENT) {
            @Override
            public void onAction(Intent intent, String action) {
                if (getPayloadObject(intent, SCREEN, MdsActivity.class) instanceof StoryActivity) {
                    send(getContext(), Ui.STORY_BOOKMARKS_CONSIDERED_INTENT);
                }
            }
        };
    }

    private void startStory(Story story, Integer offset) {
        stopStory();

        storyPlayer = isStreamCached(this, story)
                ? new CachedPlayer(this, new DataSource(story, offset))
                : new NetworkPlayer(this, new DataSource(story, offset));

        cacheManager.setStory(story);
        settingsManager.setStoryPlaying(settingsManager.isAutoPlay());
        send(this, Player.STARTED_EVENT, story);
    }

    @Override
    public void unsubscribe() {
        receiver.unsubscribe();
        super.unsubscribe();
    }
}
