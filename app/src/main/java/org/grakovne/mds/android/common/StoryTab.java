package org.grakovne.mds.android.common;

public class StoryTab {
    public static final String ALL_STORIES = "all";
    public static final String BEST_STORIES = "best";
    public static final String RECENT_STORIES = "recent";
}
