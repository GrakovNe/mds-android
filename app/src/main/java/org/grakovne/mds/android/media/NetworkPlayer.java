package org.grakovne.mds.android.media;

import android.content.Context;

import static org.grakovne.mds.android.utils.NetworkUtils.findStoryUrl;

public class NetworkPlayer extends StoryPlayer {

    public NetworkPlayer(Context context, DataSource dataSource) {
        super(context, dataSource);
    }

    @Override
    public String toUri(DataSource source) {
        return findStoryUrl(source);
    }

    @Override
    public void onConfigured(DataSource source) {

    }
}

