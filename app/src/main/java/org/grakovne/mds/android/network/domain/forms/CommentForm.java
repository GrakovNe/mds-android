package org.grakovne.mds.android.network.domain.forms;

import java.io.Serializable;

public class CommentForm implements Serializable {
    private String message;
    private String userName;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
