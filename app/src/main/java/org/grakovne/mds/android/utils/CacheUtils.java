package org.grakovne.mds.android.utils;

import android.content.Context;
import android.os.Environment;

import org.grakovne.mds.android.network.domain.responses.Story;

import java.io.File;

public class CacheUtils {

    public static File buildStreamFile(Context context, Story story) {
        return new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC), story.getId().toString());
    }

    public static Boolean isStreamCached(Context context, Story story) {
        return CacheUtils.isStreamExists(context, story) && CacheUtils.isStreamValid(context, story);
    }

    private static Boolean isStreamExists(Context context, Story story) {
        return buildStreamFile(context, story).exists();
    }

    private static Boolean isStreamValid(Context context, Story story) {
        return story.getFileSize().equals(buildStreamFile(context, story).length());
    }
}
