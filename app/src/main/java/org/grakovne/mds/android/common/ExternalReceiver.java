package org.grakovne.mds.android.common;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import static org.grakovne.mds.android.MdsApplication.APP_TAG;

public abstract class ExternalReceiver extends Receiver {

    public ExternalReceiver(Context context, String... actions) {
        super(context);

        for (String action : actions) {
            context.registerReceiver(this, new IntentFilter(action));
        }
    }

    @Override
    public void unsubscribe() {
        super.unsubscribe();

        try {
            context.unregisterReceiver(this);
        } catch (Exception ex) {
            Log.w(APP_TAG, "Can't stop " + this.toString());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    public abstract void onAction(Intent intent, String action);
}
