package org.grakovne.mds.android.media;

import org.grakovne.mds.android.network.domain.responses.Story;

import java.io.Serializable;

public class PlayingState implements Serializable {

    private final Story story;
    private final Integer duration;
    private final Integer position;

    public PlayingState(Story story,
                        Integer duration,
                        Integer position) {

        this.story = story;
        this.duration = duration;
        this.position = position;
    }

    public Story getStory() {
        return story;
    }

    public Integer getDuration() {
        return duration;
    }

    public Integer getPosition() {
        return position;
    }

    public Integer getNextPosition() {
        return position == null ? 0 : position + 1;
    }
}
