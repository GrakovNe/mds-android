package org.grakovne.mds.android.common;

public class PayloadNames {

    public static final String BASE_ACTION_PREFIX = "org.grakovne.mds.android.";

    public static final String STORY = BASE_ACTION_PREFIX + "story";
    public static final String STORY_PAGE_NUMBER = BASE_ACTION_PREFIX + "story_page_number";
    public static final String STORY_PAGE = BASE_ACTION_PREFIX + "story_page";

    public static final String USER = BASE_ACTION_PREFIX + "user";
    public static final String USER_FORM = BASE_ACTION_PREFIX + "user_form";
    public static final String STORY_IS_EDGE_PAGE = BASE_ACTION_PREFIX + "story_is_last_page";
    public static final String PLAYING_STATE = BASE_ACTION_PREFIX + "playing_state";
    public static final String TIMER_SETTING = BASE_ACTION_PREFIX + "timer_settings";

    public static final String METRICS = BASE_ACTION_PREFIX + "METRICS";

    public static final String RATING = BASE_ACTION_PREFIX + "story_rating";
    public static final String NOTIFICATION = BASE_ACTION_PREFIX + "notification";
    public static final String SCREEN = BASE_ACTION_PREFIX + "screen";
    public static final String STREAM = BASE_ACTION_PREFIX + "stream";
}
