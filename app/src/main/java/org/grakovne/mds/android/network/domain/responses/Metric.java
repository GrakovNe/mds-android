package org.grakovne.mds.android.network.domain.responses;

import java.io.Serializable;

public class Metric implements Serializable {
    private Long stories = 0L;
    private Long authors = 0L;
    private Long comments = 0L;
    private Long listenedStories = 0L;
    private Long listenedTime = 0L;
    private Long finishedStories = 0L;
    private Long ratedStories = 0L;

    public Long getStories() {
        return stories;
    }

    public void setStories(Long stories) {
        this.stories = stories;
    }

    public Long getAuthors() {
        return authors;
    }

    public void setAuthors(Long authors) {
        this.authors = authors;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public Long getListenedStories() {
        return listenedStories;
    }

    public void setListenedStories(Long listenedStories) {
        this.listenedStories = listenedStories;
    }

    public Long getListenedTime() {
        return listenedTime;
    }

    public void setListenedTime(Long listenedTime) {
        this.listenedTime = listenedTime;
    }

    public Long getFinishedStories() {
        return finishedStories;
    }

    public void setFinishedStories(Long finishedStories) {
        this.finishedStories = finishedStories;
    }

    public Long getRatedStories() {
        return ratedStories;
    }

    public void setRatedStories(Long ratedStories) {
        this.ratedStories = ratedStories;
    }
}
