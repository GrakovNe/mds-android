package org.grakovne.mds.android.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.PowerManager;

import org.grakovne.mds.android.common.ActionNames.Player;

import java.io.IOException;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
import static org.grakovne.mds.android.common.ActionNames.Network;
import static org.grakovne.mds.android.common.ActionNames.Player.FINISHED_INTENT;
import static org.grakovne.mds.android.manager.BroadcastManager.send;
import static org.grakovne.mds.android.utils.CommonUtils.emptyAction;
import static org.grakovne.mds.android.utils.CommonUtils.ifUserLogged;

public abstract class StoryPlayer {

    protected final Context context;
    protected DataSource dataSource;
    protected MediaPlayer mediaPlayer;
    protected AudioManager audioManager;
    protected AudioManager.OnAudioFocusChangeListener focusChangeListener;

    protected StoryPlayer(Context context, DataSource dataSource) {
        this.dataSource = dataSource;
        this.context = context;

        initPlayer();
    }

    public abstract String toUri(DataSource source);

    public abstract void onConfigured(DataSource source);

    public void play() {
        if (null == mediaPlayer) {
            return;
        }

        requestFocus();
        setVolume(1.0f);

        mediaPlayer.start();
    }

    public void pause() {
        if (null == mediaPlayer) {
            return;
        }

        mediaPlayer.pause();
        abandonFocus();
    }

    public void stop() {
        if (null == mediaPlayer) {
            return;
        }

        mediaPlayer.stop();
        mediaPlayer.seekTo(0);
        abandonFocus();
    }

    private void setVolume(float level) {
        if (null == mediaPlayer) {
            return;
        }

        mediaPlayer.setVolume(level, level);
    }

    private void abandonFocus() {
        if (null == audioManager) {
            return;
        }

        audioManager.abandonAudioFocus(focusChangeListener);
    }

    private void requestFocus() {
        if (null == audioManager || null == mediaPlayer) {
            return;
        }

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        audioManager.requestAudioFocus(focusChangeListener,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN
        );
    }

    private Integer getPosition() {
        return null != mediaPlayer ? mediaPlayer.getCurrentPosition() / 1000 : 0;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public PlayingState getCurrentPlayingState() {
        return new PlayingState(
                getDataSource().getStory(),
                getDataSource().getStory().getLength(),
                getPosition() + getDataSource().getOffset());
    }

    private void initPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        focusChangeListener = state -> {
            if (null == mediaPlayer || null == dataSource) {
                return;
            }

            switch (state) {
                case AUDIOFOCUS_LOSS:
                    send(context, Player.PAUSED_INTENT);
                    break;
                case AUDIOFOCUS_LOSS_TRANSIENT:
                    send(context, Player.PAUSED_INTENT);
                    break;
                case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    setVolume(0.3f);
                    break;
                case AUDIOFOCUS_GAIN:
                    setVolume(1.0f);
                    play();
                    break;
            }
        };

        mediaPlayer.setOnErrorListener((mp, what, extra) -> {
            send(context, Player.STOPPED_EVENT);
            return true;
        });

        mediaPlayer.setOnPreparedListener(mp -> {
            onConfigured(dataSource);
            send(context, Player.PREPARED_EVENT);
        });

        mediaPlayer.setOnCompletionListener(mp -> {
            PlayingState state = getCurrentPlayingState();

            if (state.getNextPosition() < dataSource.getStory().getLength()) {
                ifUserLogged(context, () -> send(context, Network.STORY_BOOKMARKED_INTENT, getCurrentPlayingState()), emptyAction());
                send(context, Player.STARTED_INTENT, dataSource.getStory(), state);
            } else {
                send(context, FINISHED_INTENT);
            }
        });

        try {
            mediaPlayer.setDataSource(toUri(dataSource));
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            send(context, Player.STOPPED_EVENT);
        }
    }
}
