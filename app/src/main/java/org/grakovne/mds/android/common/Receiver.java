package org.grakovne.mds.android.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import static org.grakovne.mds.android.MdsApplication.APP_TAG;
import static org.grakovne.mds.android.utils.CommonUtils.ifDebugEnabled;
import static org.grakovne.mds.android.utils.CommonUtils.ifNotNull;

public abstract class Receiver extends BroadcastReceiver {
    protected final Context context;

    public Receiver(Context context, String... actions) {
        this.context = context;
        subscribe(actions);

    }

    public void subscribe(String... actions) {
        for (String action : actions) {
            LocalBroadcastManager.getInstance(context).registerReceiver(this, new IntentFilter(action));
        }
    }

    public void unsubscribe() {
        try {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
        } catch (Exception ex) {
            Log.w(APP_TAG, "Can't stop " + this.toString());
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ifDebugEnabled(() -> Log.d(APP_TAG, "Catch message: " + intent.getAction()));
        ifNotNull(intent.getAction(), () -> onAction(intent, intent.getAction()));
    }

    public abstract void onAction(Intent intent, String action);
}
